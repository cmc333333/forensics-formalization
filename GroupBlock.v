Require Export Coq.NArith.BinNat.
Require Export utils.

Local Open Scope N_scope. (* 1024 is now a binary number, N, rather than a nat *)

(* BA = "Block Address" *)
Structure GroupBlock := mkGroupDesc {
  inodeBitmapBA:N; (* Offset 4 *)
  inodeTableBA:N (* Offset 8 *)
}.

Function groupBlockFrom (diskImage: list N) (groupId: N) :=
  (* Account for bootable space and superblock; each descriptor is 32 bytes *)
  let groupDescPos := 2048 + groupId/8 in
  (* Allocation Bitmap *)
  (* BitmapBlock is located at the group descriptor's offset + 4 *)
  opt_flatmap (int32 diskImage (groupDescPos+4)) (fun inodeBitmapBA =>
    opt_flatmap (int32 diskImage (groupDescPos+8)) (fun inodeTableBA =>
      Some (mkGroupDesc inodeBitmapBA inodeTableBA)
  )).
