Require Import Coq.ZArith.ZArith.

Require Import Disk.
Require Import File.
Require Import Util.

Open Local Scope Z.

Fixpoint magicNumbers (offsetXValues:list (Z*Z)) (file: File) : bool :=
  match offsetXValues with
  | (offset, expectedValue) :: tail => match [[ file @[ offset ] ;; diskValue |>
    magicNumbers tail file ]] with
    | value v => v
    | error => false
    end
  | _ => true
end.

Function bool_isJpeg (file: File):=  magicNumbers (
  (0, 255) :: (1, 216) :: ((fileSize file) - 2, 255) :: ((fileSize file) - 1, 217) :: nil
) file.

Function bool_isTgz (file: File) := magicNumbers ((0, 31) :: (1, 139) :: (2, 8) :: nil) file.

Definition isJpeg (file: File) :=
  file @[ 0 ] = value 255
  /\ file @[ 1 ] = value 216 
  /\ file @[ -2 ] = value 255
  /\ file @[ -1 ] = value 217.

Definition isGzip (file: File) :=
  file @[ 0 ] = value 31
  /\ file @[ 1 ] = value 139 
  /\ file @[ 2 ] = value 8.

Definition isElf (file: File) :=
  file @[ 0 ] = value 127
  /\ file @[ 1 ] = value 69 
  /\ file @[ 2 ] = value 76
  /\ file @[ 3 ] = value 70.

Lemma jpeg_is_not_gzip : forall (file: File),
  (isJpeg file) -> ~ (isGzip file).
  Proof.
  unfold isGzip, isJpeg.
  intros file jpeg_asmpt.
  destruct jpeg_asmpt as [byte0_is_255]. rewrite byte0_is_255.
  unfold not. intros contra. destruct contra as [not_equal].
  discriminate not_equal.
  Qed.
