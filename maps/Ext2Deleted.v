Require Import Coq.ZArith.ZArith.

Require Import Disk.
Require Import Util.
Require Import GroupDescriptor.
Require Import SuperBlock.


Open Local Scope Z.

Definition Ext2IsDeletedInode (inode : Z) (disk : Disk): Exc bool :=
  [[ ext2_superblock_from disk ;; superblock =>
    let groupId := inode / (numInodesInBlockGroup superblock) in
    (* groupIds are 1 indexed *)
    [[ ext2_group_descriptor_at disk (1 + groupId) ;; group_descriptor =>
      let inodeIndexInGroup := inode mod (numInodesInBlockGroup superblock) in
      let bitmapPos := (blockAddr2Offset superblock (startBlockAddrOfInodeBitmap group_descriptor)) in
      (* Byte associated with the INode Bitmap *)
      [[ disk (bitmapPos + (inodeIndexInGroup/8)) ;; inodeBitmapByte |>
        (andb
          (* Valid INode - Must be less than the number of INodes *)
          (inode <=? (numInodes superblock))

          (* The bit associated with this inode is 0 *)
          (negb (Z.testbit inodeBitmapByte (inodeIndexInGroup mod 8)))
        )
      ]]
    ]]
  ]]
.
