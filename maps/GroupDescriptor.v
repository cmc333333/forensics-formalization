Require Import Coq.ZArith.ZArith.

Require Import Disk.
Require Import Util.

Open Local Scope Z.

Structure ext2_group_descriptor_t := mk_ext2_group_descriptor_t {
  startBlockAddrOfBlockBitmap : Z;
  startBlockAddrOfInodeBitmap : Z;
  startBlockAddrOfInodeTable : Z;
  numUnallocatedBlocksInGroup : Z;
  numUnallocatedInodesInGroup : Z;
  numDirsInGroup : Z;
  calcBlockGroupNumber : option Z;
  calcNumBlocksInGroup : option Z
}.

(* a function to parse a single Ext2 group descriptor, and build a structure/record from it *)
Definition ext2_group_descriptor_parse (m : Disk) : Exc ext2_group_descriptor_t := 
  [[ subsequence_list_lendu m 0 4 ;; startBlockAddrOfBlockBitmap =>
    [[ subsequence_list_lendu m 4 4 ;; startBlockAddrOfInodeBitmap =>
      [[ subsequence_list_lendu m 8 4 ;; startBlockAddrOfInodeTable =>
        [[ subsequence_list_lendu m 12 2 ;; numUnallocatedBlocksInGroup =>
          [[ subsequence_list_lendu m 14 2 ;; numUnallocatedInodesInGroup =>
            [[ subsequence_list_lendu m 16 2 ;; numDirsInGroup |>
              {|
                startBlockAddrOfBlockBitmap := startBlockAddrOfBlockBitmap;
                startBlockAddrOfInodeBitmap := startBlockAddrOfInodeBitmap;
                startBlockAddrOfInodeTable := startBlockAddrOfInodeTable;
                numUnallocatedBlocksInGroup := numUnallocatedBlocksInGroup;
                numUnallocatedInodesInGroup := numUnallocatedInodesInGroup;
                numDirsInGroup := numDirsInGroup;
                calcBlockGroupNumber := None;
                calcNumBlocksInGroup := None
                |}
            ]]
          ]]
        ]]
      ]]
    ]]
  ]].

Function ext2_group_descriptor_at (disk : Disk) (groupId : Z) :=
  (* Account for bootable space and superblock; each descriptor is 32 bytes *)
  (* One-indexed *)
  let groupDescPos := 2048 + ( (groupId - 1) * 32) in
  ext2_group_descriptor_parse (shift disk groupDescPos).
