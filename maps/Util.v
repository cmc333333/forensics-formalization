Require Import Coq.PArith.BinPos.
Require Import Coq.ZArith.ZArith.
Require Export List.

Open Scope Z.

(* combining Exc computations *)
Definition opt_flatmap {A B : Type} (opt : Exc A) (f : A -> Exc B) :=
  match opt with
    | Some v => f v
    | None => None
  end.

Definition opt_map {A B : Type} (opt : Exc A) (f : A -> B) :=
  match opt with
    | Some v => value (f v)
    | None => None
  end.

Infix "_flatmap_" := opt_flatmap (at level 50).
Infix "_map_" := opt_map (at level 50).

(* notation to make it easier to read/write the combination of Exc computations, based on Chris' pattern (or Haskell do notation) *)
Notation "[[ e1 ;; x => e2 ]]" := (opt_flatmap e1 (fun x => e2)).
Notation "[[ e1 ;; x |> e2 ]]" := (opt_map e1 (fun x => e2)).

(* from an image m given as a function Z -> Exc Z, extract the list of elements
  [ m(start); m(start+1); ...; m(start+length-1) ] *)
Definition subsequence_list (m : Z -> Exc Z) (start : Z) (length : Z) : Exc (list Z) :=
  match length with
    | Z0 => value nil
    | Zpos l => 
      (Pos.peano_rec
        (fun _ => Z -> Exc (list Z))
        (fun (start' : Z) => [[ m start' ;; x |> cons x nil ]])
        (fun _ (subsequence_list_aux_pred_l : Z -> Exc (list Z)) (start' : Z) => 
          [[ m start' ;; x => 
            [[ subsequence_list_aux_pred_l (Z.succ start') ;; xs |>
              cons x xs
            ]]
          ]]
        )
      )
      l
      start
    | Zneg _ => error
  end.

(* little endian unsigned *)
Fixpoint lendu (l : list Z) : Z := 
match l with 
  | nil => 0%Z
  | a::l' => (a + (256 * lendu l'))%Z
end.   

Definition subsequence_list_lendu (m : Z -> Exc Z) (offset : Z) (length : Z) : Exc Z := [[ subsequence_list m offset length ;; xs |> lendu xs ]].

Lemma strip_value: forall (A: Type) (o: option A), o <> error ->
  exists (v: A), o = value v.
Proof.
  intros A o Hneq.
  destruct o; [ exists a; reflexivity | contradict Hneq; reflexivity ].
Qed.

Function range (start: Z) (exclusiveEnd: Z) : list Z :=
  N.peano_rect
  (fun _ => Z -> list Z) (* Signature of recursive calls *)
  (fun (start: Z) => nil) (* Base case *)
  (fun (counter: N) (rec: Z -> list Z) (start: Z) =>
    if (exclusiveEnd <=? start)
    then nil
    else start :: (rec (start + 1)))
  (Z.to_N (exclusiveEnd - start))
  start.

Infix "upto" := range (at level 50).

Fixpoint takeWhile {A} (predicate: A->bool) (lst: list A) : list A :=
  match lst with
  | head :: tail => if (predicate head) 
                    then (head :: (takeWhile predicate tail))
                    else nil
  | nil => nil
end.

Fixpoint flatten {A} (lst: list (Exc A)): list A :=
  match lst with
    | (value head) :: tail => head :: (flatten tail)
    | error :: tail => flatten tail
    | nil => nil
  end.
