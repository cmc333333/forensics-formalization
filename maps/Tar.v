Require Import Coq.Lists.List.
Require Import Coq.ZArith.ZArith.

Require Import Disk.
Require Import File.
Require Import Util.

Local Open Scope Z.

Definition ByteString := list Z.

Fixpoint fromOctalAscii (bytes: list Z) : Exc Z :=
  match bytes with
  | nil => value 0
  | byte :: tail => match (fromOctalAscii tail) with
    | error => error
    | value rest => if (andb (48 <=? byte) (byte <=? 56))
      then value (rest + ((byte-48) * (8 ^ (Z.of_nat (length tail)))))
      else error (* Invalid character *)
    end
  end.

Function parseFirstFileNameAndFile (tar: File) : Exc (ByteString*File) :=
  let firstHundredBytes := (map tar.(data) (0 upto 100)) in
  let fileName := flatten (takeWhile (fun (byte: Exc Z) => match byte with
  | error => false
  | value 0 => false
  | value _ => true
  end) firstHundredBytes) in
  (* File Size is encoded in octal, represented as ASCII characters. It begins
  at offset 124 and runs for 11 characters *)
  [[ subsequence_list tar.(data) 124 11 ;; fileSizeList =>
    [[ fromOctalAscii fileSizeList ;; fileSize |>
      (fileName, (mkFile fileSize
                         tar.(deleted)  (* Keep the deleted status of the tar *)
                         (shift tar.(data) 512))) (* Header size = 512 *)
    ]]
  ]].

Function recFileNameFrom (nextCall: File -> list ByteString) (remaining: File)
  : list ByteString :=
  if (remaining.(fileSize) <? 0)
    then nil
  else match (parseFirstFileNameAndFile remaining) with
    | error => nil
    | value (fileName, file) =>
        (* Strip the first file out of the tar *)
        (* Round to the nearest 512 *)
        let firstFileSize := (
          if (file.(fileSize) mod 512 =? 0)
          then file.(fileSize) + 512
          else 512 * (2 + (file.(fileSize) / 512))) in
        let trimmedTar := (mkFile (remaining.(fileSize) - firstFileSize)
                                  remaining.(deleted) (* Keep parent's value *)
                                  (shift remaining.(data) firstFileSize)) in
        fileName :: (nextCall trimmedTar)
    end.

Function parseFileNames (file: File): list (list Z) :=
  N.peano_rect
  (fun _ => File -> list (list Z)) (* Signature of recursive calls *)
  (fun (nilFile: File) => nil:(list (list Z))) (* Base case -- empty file *)
  (fun (prev: N) (rec: File -> list (list Z)) (remainingFile: File) =>
    recFileNameFrom rec remainingFile)  (* Recursive case -- more file remaining *)
  (Z.to_N file.(fileSize))
  file.
