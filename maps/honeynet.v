Require Import Coq.Lists.List.
Require Import Coq.ZArith.ZArith.

Require Import Disk.
Require Import File.
Require Import FileNames.
Require Import FileSystem.
Require Import FileType.
Require Import Inode.
Require Import Tar.
Require Import Timeline.
Require Import Util.

Require Import example_images.

Open Local Scope Z.

Definition honeynet_image_a := Disk.mkDiskFrom honeynet_map.
(* All gunzip operations return the file mentioned *)
Definition gunzip_a := (fun (input: File) => 
  (mkFile 1454080 input.(deleted) (Disk.mkDiskFrom gunzipped_23))).

Lemma lee_honeynet_file:
  (Timeline.isSound 
    ( 
      (* Mar 16 01 12:36:48 *)
      (FileModification 984706608 23)  (* rootkit lk.tar.gz downloaded *)
      (* Mar 16 01 12:44:50 *)
      :: (FileAccess 984707090 23)     (* Gunzip and Untar rootkit lk.tar.gz *)
      :: (FileAccess 984707102 30130)  (* change ownership of rootkit files to root.root *)
      :: (FileDeletion 984707102 30188)(* deletion of original /bin/netstat *)
      :: (FileCreation 984707102 2056) (* insertion of trojan netstat *)
      :: (FileDeletion 984707102 30191)(* deletion of original /bin/ps *)
      :: (FileCreation 984707102 2055) (* insertion of trojan ps *)
      :: (FileDeletion 984707102 48284)(* deletion of origin /sbin/ifconfig *)
      :: (FileCreation 984707102 2057) (* insertion of trojan ifconfig *)
      (* Mar 16 01 12:45:03 *)
      :: (FileAccess 984707103 30131)  (* the copy of service files to /etc *)
      :: (FileCreation 984707103 26121)(* hackers services file copied on top of original *)
      (* Mar 16 01 12:45:05 *)
      :: (FileDeletion 984707105 23)   (* deletion of rootkit lk.tar.gz *)
      :: nil
    )
    honeynet_image_a
  ).
Proof.
  unfold isSound. split.

  (* foundOn - verify each event *)
  unfold foundOn. intros. simpl in H.
  repeat (destruct H; [ symmetry in H; rewrite H; vm_compute; reflexivity |]).
  exfalso. apply H.

  (* monotonically increasing *)
  intros index.
  (* index < length of list (-1) *)
  repeat (destruct index; [vm_compute; intros; discriminate x0 |]).
  (* index is outside of the list *)
  vm_compute. intros.
  repeat (progress (apply le_pred in x; simpl in x;
                    try (contradict x; apply le_Sn_0))).
Qed.

Lemma bordland_honeynet_file:
  exists (file: File),
  (isOnDisk file honeynet_image_a)
  /\ isDeleted file
  /\ isGzip file
  /\ exists (filename: ByteString),
     (In filename (Tar.parseFileNames (gunzip_a file)))
     /\ looksLikeRootkit filename.
Proof.
  set (to_compute := Inode.parseFileFromInodeIndex honeynet_image_a 23 (0 :: 1 :: 2 :: nil)).
  pose (strip_value _ to_compute) as stripped.
  destruct stripped as [file].
  vm_compute in to_compute. discriminate.

  exists file.

  split. unfold isOnDisk.
  exists 23. exists (0 :: 1 :: 2 :: nil).
  symmetry in H. rewrite H.
  subst to_compute. reflexivity.


  split. 
  unfold isDeleted.
  symmetry in H. 
  vm_compute in to_compute. subst to_compute.
  inversion H. simpl. reflexivity.

  split. 
  symmetry in H.
  vm_compute in to_compute. subst to_compute.
  inversion H. vm_compute. 
  repeat (split; [reflexivity | ]); reflexivity.

  exists (ascii2Bytes "last/ssh").
    split. 

    vm_compute. 
    repeat (try (left; reflexivity) ; right).

    vm_compute.
    repeat (try (left; reflexivity) ; right).
Qed.
