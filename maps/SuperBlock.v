Require Import Coq.ZArith.ZArith.

Require Import Disk.
Require Import Util.

Open Local Scope Z.

Structure ext2_superblock_t := mk_ext2_superblock_t {
  numInodes : Z;
  numBlocks : Z;
  numBlocksReserved : Z;
  numUnallocatedBlocks : Z;
  numUnallocatedInodes : Z;
  blockWhereGroupZeroStarts : Z;
  blockSizeExponent : Z;
  fragmentSize : Z;
  numBlocksInBlockGroup : Z;
  numFragmentsInBlockGroup : Z;
  numInodesInBlockGroup : Z;
  lastMountTime : Z;
  lastWrittenTime : Z;
  currentMountCount : Z;
  maxMountCount : Z;
  signature : Z;
  fileSystemState : Z;
  errorHandlingMethod : Z;
  minorVersion : Z;
  lastConsistencyCheckTime : Z;
  intervalBetweenForcedConsistencyChecks : Z;
  creatorOS : Z;
  majorVersion : Z;
  uidThatCanUseReservedBlocks : Z;
  gidThatCanUseReservedBlocks : Z;
  firstNonReservedInodeInFS : Z;
  sizeInodeStructure : Z;
  blockGroupThatThisSuperBlockIsPartOf : Z;
  compatibleFeatureFlags : Z;
  incompatibleFeatureFlags : Z;
  readOnlyFeatureFlags : Z;
  fileSystemID : list Z;
  volumeName : list Z;
  pathLastMountedOn : list Z;
  algorithmUsageBitmap : Z;
  numBlocksToPreallocateForFiles : Z;
  numBlocksToPreallocateForDirs : Z;
  journalID : Z;
  journalInode : Z;
  journalDevice : Z;
  headOfOrphanInodeList : Z
}.

(* a function to parse an Ext2 superblock, and build a structure/record from it *)
Definition ext2_superblock_parse (m : Disk) : Exc ext2_superblock_t := 
  [[ subsequence_list_lendu m 0 4 ;; numInodes =>
    [[ subsequence_list_lendu m 4 4 ;; numBlocks =>
      [[ subsequence_list_lendu m 8 4 ;; numBlocksReserved =>
        [[ subsequence_list_lendu m 12 4 ;; numUnallocatedBlocks =>
          [[ subsequence_list_lendu m 16 4 ;; numUnallocatedInodes =>
            [[ subsequence_list_lendu m 20 4 ;; blockWhereGroupZeroStarts =>
              [[ subsequence_list_lendu m 24 4 ;; blockSizeExponent =>
                [[ subsequence_list_lendu m 28 4 ;; fragmentSize =>
                  [[ subsequence_list_lendu m 32 4 ;; numBlocksInBlockGroup =>
                    [[ subsequence_list_lendu m 36 4 ;; numFragmentsInBlockGroup =>
                      [[ subsequence_list_lendu m 40 4 ;; numInodesInBlockGroup =>
                        [[ subsequence_list_lendu m 44 4 ;; lastMountTime =>
                          [[ subsequence_list_lendu m 48 4 ;; lastWrittenTime =>
                            [[ subsequence_list_lendu m 52 2 ;; currentMountCount =>
                              [[ subsequence_list_lendu m 54 2 ;; maxMountCount =>
                                [[ subsequence_list_lendu m 56 2 ;; signature =>
                                  [[ subsequence_list_lendu m 58 2 ;; fileSystemState =>
                                    [[ subsequence_list_lendu m 60 2 ;; errorHandlingMethod =>
                                      [[ subsequence_list_lendu m 62 2 ;; minorVersion =>
                                        [[ subsequence_list_lendu m 64 4 ;; lastConsistencyCheckTime =>
                                          [[ subsequence_list_lendu m 68 4 ;; intervalBetweenForcedConsistencyChecks =>
                                            [[ subsequence_list_lendu m 72 4 ;; creatorOS =>
                                              [[ subsequence_list_lendu m 76 4 ;; majorVersion =>
                                                [[ subsequence_list_lendu m 80 2 ;; uidThatCanUseReservedBlocks =>
                                                  [[ subsequence_list_lendu m 82 2 ;; gidThatCanUseReservedBlocks =>
                                                    [[ subsequence_list_lendu m 84 4 ;; firstNonReservedInodeInFS =>
                                                      [[ subsequence_list_lendu m 88 2 ;; sizeInodeStructure =>
                                                        [[ subsequence_list_lendu m 90 2 ;; blockGroupThatThisSuperBlockIsPartOf =>
                                                          [[ subsequence_list_lendu m 92 4 ;; compatibleFeatureFlags =>
                                                            [[ subsequence_list_lendu m 96 4 ;; incompatibleFeatureFlags =>
                                                              [[ subsequence_list_lendu m 100 4 ;; readOnlyFeatureFlags =>
                                                                [[ subsequence_list m 104 16 ;; fileSystemID =>
                                                                  [[ subsequence_list m 120 16 ;; volumeName =>
                                                                    [[ subsequence_list m 136 64 ;; pathLastMountedOn =>
                                                                      [[ subsequence_list_lendu m 200 4 ;; algorithmUsageBitmap =>
                                                                        [[ subsequence_list_lendu m 204 1 ;; numBlocksToPreallocateForFiles =>
                                                                          [[ subsequence_list_lendu m 205 1 ;; numBlocksToPreallocateForDirs =>
                                                                            [[ subsequence_list_lendu m 208 16 ;; journalID =>
                                                                              [[ subsequence_list_lendu m 224 4 ;; journalInode =>
                                                                                [[ subsequence_list_lendu m 228 4 ;; journalDevice =>
                                                                                  [[ subsequence_list_lendu m 232 4 ;; headOfOrphanInodeList |>
                                                                                    {|
                                                                                      numInodes := numInodes;
                                                                                      numBlocks := numBlocks;
                                                                                      numBlocksReserved := numBlocksReserved;
                                                                                      numUnallocatedBlocks := numUnallocatedBlocks;
                                                                                      numUnallocatedInodes := numUnallocatedInodes;
                                                                                      blockWhereGroupZeroStarts := blockWhereGroupZeroStarts;
                                                                                      blockSizeExponent := blockSizeExponent;
                                                                                      fragmentSize := fragmentSize;
                                                                                      numBlocksInBlockGroup := numBlocksInBlockGroup;
                                                                                      numFragmentsInBlockGroup := numFragmentsInBlockGroup;
                                                                                      numInodesInBlockGroup := numInodesInBlockGroup;
                                                                                      lastMountTime := lastMountTime;
                                                                                      lastWrittenTime := lastWrittenTime;
                                                                                      currentMountCount := currentMountCount;
                                                                                      maxMountCount := maxMountCount;
                                                                                      signature := signature;
                                                                                      fileSystemState := fileSystemState;
                                                                                      errorHandlingMethod := errorHandlingMethod;
                                                                                      minorVersion := minorVersion;
                                                                                      lastConsistencyCheckTime := lastConsistencyCheckTime;
                                                                                      intervalBetweenForcedConsistencyChecks := intervalBetweenForcedConsistencyChecks;
                                                                                      creatorOS := creatorOS;
                                                                                      majorVersion := majorVersion;
                                                                                      uidThatCanUseReservedBlocks := uidThatCanUseReservedBlocks;
                                                                                      gidThatCanUseReservedBlocks := gidThatCanUseReservedBlocks;
                                                                                      firstNonReservedInodeInFS := firstNonReservedInodeInFS;
                                                                                      sizeInodeStructure := sizeInodeStructure;
                                                                                      blockGroupThatThisSuperBlockIsPartOf := blockGroupThatThisSuperBlockIsPartOf;
                                                                                      compatibleFeatureFlags := compatibleFeatureFlags;
                                                                                      incompatibleFeatureFlags := incompatibleFeatureFlags;
                                                                                      readOnlyFeatureFlags := readOnlyFeatureFlags;
                                                                                      fileSystemID := fileSystemID;
                                                                                      volumeName := volumeName;
                                                                                      pathLastMountedOn := pathLastMountedOn;
                                                                                      algorithmUsageBitmap := algorithmUsageBitmap;
                                                                                      numBlocksToPreallocateForFiles := numBlocksToPreallocateForFiles;
                                                                                      numBlocksToPreallocateForDirs := numBlocksToPreallocateForDirs;
                                                                                      journalID := journalID;
                                                                                      journalDevice := journalDevice;
                                                                                      journalInode := journalInode;
                                                                                      headOfOrphanInodeList := headOfOrphanInodeList
                                                                                      |}
                                                                                  ]]
                                                                                ]]
                                                                              ]]
                                                                            ]]
                                                                          ]]
                                                                        ]]
                                                                      ]]
                                                                    ]]
                                                                  ]]
                                                                ]]
                                                              ]]
                                                            ]]
                                                          ]]
                                                        ]]
                                                      ]]
                                                    ]]
                                                  ]]
                                                ]]
                                              ]]
                                            ]]
                                          ]]
                                        ]]
                                      ]]
                                    ]]
                                  ]]
                                ]]
                              ]]
                            ]]
                          ]]
                        ]]
                      ]]
                    ]]
                  ]]
                ]]
              ]]
            ]]
          ]]
        ]]
      ]]
    ]]
  ]].

Function blockSize (superblock: ext2_superblock_t) := Z.shiftl 1024 (blockSizeExponent superblock).

Function ext2_superblock_from (disk : Disk) := ext2_superblock_parse (shift disk 1024).

Function blockAddr2Offset (superblock: ext2_superblock_t) (blockAddress: Z) := 2048 + (blockSize superblock) * (blockAddress - 2).
