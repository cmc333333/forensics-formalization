Require Import Coq.ZArith.ZArith.

Require Import Disk.
Require Import Inode.
Require Import Util.

Inductive Event: Type :=
  | FileAccess: Z -> Z -> Event
  | FileModification: Z -> Z -> Event
  | FileCreation: Z -> Z -> Event
  | FileDeletion: Z -> Z -> Event
  (*| Execution: ByteString -> Event *).

Definition Timeline: Type := list Event.

Function timestampOf (event: Event) : Exc Z :=
  match event with
  | FileAccess timestamp _ => value timestamp
  | FileModification timestamp _ => value timestamp
  | FileCreation timestamp _ => value timestamp
  | FileDeletion timestamp _ => value timestamp
  end.

Function inodeIndexOf (event: Event) : Exc Z :=
  match event with
  | FileAccess _ inodeIndex => value inodeIndex
  | FileModification _ inodeIndex => value inodeIndex
  | FileCreation _ inodeIndex => value inodeIndex
  | FileDeletion _ inodeIndex => value inodeIndex
  end.

Definition beforeOrConcurrent (lhs rhs: Event) :=
  match (timestampOf lhs, timestampOf rhs) with
  | (value lhs_time, value rhs_time) => lhs_time <= rhs_time
  | _ => False
  end.

Definition foundOn (event: Event) (disk: Disk) : Prop :=
  match (inodeIndexOf event) _flatmap_ (fun inodeIndex =>
    (ext2_inode_from disk inodeIndex) _map_ (fun inode =>
      match event with
      | FileAccess timestamp _ => inode.(accessTime) = timestamp
      | FileModification timestamp _ => inode.(modificationTime) = timestamp
      | FileCreation timestamp _ => inode.(changeTime) = timestamp
      | FileDeletion timestamp _ => inode.(deletionTime) = timestamp
      end
    )
  ) with
  | error => False
  | value prop => prop
  end.

Definition isSound (timeline: Timeline) (disk: Disk) :=
  (forall (event: Event),
    (In event timeline) -> (foundOn event disk))
  /\ (forall (index: nat),
        (index < ((length timeline) - 1) )%nat ->
          match (nth_error timeline index, nth_error timeline (index + 1)) with
          | (value lhsEvent, value rhsEvent) => beforeOrConcurrent lhsEvent rhsEvent
          | _ => False
          end)
  .

