Require Import Coq.ZArith.ZArith.
Require Import FSets.FMaps.
Require Import FSets.FMapAVL.

(* instantiation of the AVL module that implements the finite map interface *)

Open Local Scope Z.

Module MZ := FSets.FMapAVL.Make Z_as_OT.

(* the type of finite maps from Z to Z *)
Definition map_Z_Z : Type := MZ.t Z.

(* lookup in a finite map *)
Definition find k (m : map_Z_Z) := MZ.find k m.

(* create a new finite map by adding/overwriting an element with an existing finite map *)
Definition update (p : Z * Z) (m : map_Z_Z) := MZ.add (fst p) (snd p) m.

(* notation for building finite maps *)
(* k |-> v   says that key k maps to value v *)
Notation "k |-> v" := (pair k v) (at level 60).
Notation "[ ]" := (MZ.empty Z).
Notation "[ p1 , .. , pn ]" := (update p1 .. (update pn []) .. ).

(* ======================================================================================= *)
(* Disk creation *)

Definition ByteData := Z -> Exc Z.
Definition Disk := ByteData.

Function mkDiskFrom (rawImage: map_Z_Z) : Disk :=
  fun (n : Z) => find n rawImage.

(* ======================================================================================= *)
(* Disk manipulation *)

(* from an image m given as a function Z -> Exc Z, create a new image that is the same as 
   m, but the value at offset n in the original image is at (n + offset) in the new image. *)
Definition shift (disk : Disk) (offset : Z) (n : Z) : Exc Z := disk (n + offset).

