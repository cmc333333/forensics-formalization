Require Import Coq.ZArith.ZArith.

Require Import Disk.

Structure File := mkFile {
  fileSize: Z;
  deleted: bool;
  data: ByteData
}.

Notation "d @[ i ]" := (d.(data) i) (at level 60).

Definition isDeleted (file: File) := file.(deleted) = true.
