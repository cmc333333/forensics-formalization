Require Import Coq.ZArith.ZArith.

Require Import Disk.
Require Import Ext2Deleted.
Require Import File.
Require Import GroupDescriptor.
Require Import SuperBlock.
Require Import Util.

Open Local Scope Z.

Structure ext2_inode_t := mk_ext2_inode_t {
  fileMode : Z;
  lowerBitsUserId : Z;
  lowerBitsFileSize : Z;
  accessTime : Z;
  changeTime : Z;
  modificationTime : Z;
  deletionTime : Z;
  lowerBitsGroupId : Z;
  linkCount : Z;
  sectorCount : Z;
  flags : Z;
  directBlockPointers : list Z;
  indirectBlockPointer : Z;
  doubleIndirectBlockPointer : Z;
  tripleIndirectBlockPointer : Z;
  generationNumber : Z;
  extendedAttributeBlock : Z;
  upperBitsFileSize : Z;
  fragmentBlockAddress : Z;
  fragmentIndexInBlock : Z;
  fragmentSize : Z;
  upperBitsUserId : Z;
  upperBitsGroupId : Z
}.

Definition ext2_inode_parse (m : Disk) : Exc ext2_inode_t :=
  [[ subsequence_list_lendu m 0 2 ;; fileMode =>
    [[ subsequence_list_lendu m 2 2 ;; lowerBitsUserId =>
      [[ subsequence_list_lendu m 4 4 ;; lowerBitsFileSize =>
        [[ subsequence_list_lendu m 8 4 ;; accessTime =>
          [[ subsequence_list_lendu m 12 4 ;; changeTime =>
            [[ subsequence_list_lendu m 16 4 ;; modificationTime =>
              [[ subsequence_list_lendu m 20 4 ;; deletionTime =>
                [[ subsequence_list_lendu m 24 2 ;; lowerBitsGroupId =>
                  [[ subsequence_list_lendu m 26 2 ;; linkCount =>
                    [[ subsequence_list_lendu m 28 4 ;; sectorCount =>
                      [[ subsequence_list_lendu m 32 4 ;; flags =>
                        (* 36-39 unused *)
                        [[ subsequence_list_lendu m 40 4 ;; directBlockPointer1 =>
                          [[ subsequence_list_lendu m 40 4 ;; directBlockPointer1 =>
                            [[ subsequence_list_lendu m 44 4 ;; directBlockPointer2 =>
                              [[ subsequence_list_lendu m 48 4 ;; directBlockPointer3 =>
                                [[ subsequence_list_lendu m 52 4 ;; directBlockPointer4 =>
                                  [[ subsequence_list_lendu m 56 4 ;; directBlockPointer5 =>
                                    [[ subsequence_list_lendu m 60 4 ;; directBlockPointer6 =>
                                      [[ subsequence_list_lendu m 64 4 ;; directBlockPointer7 =>
                                        [[ subsequence_list_lendu m 68 4 ;; directBlockPointer8 =>
                                          [[ subsequence_list_lendu m 72 4 ;; directBlockPointer9 =>
                                            [[ subsequence_list_lendu m 76 4 ;; directBlockPointer10 =>
                                              [[ subsequence_list_lendu m 80 4 ;; directBlockPointer11 =>
                                                [[ subsequence_list_lendu m 84 4 ;; directBlockPointer12 =>
                                                  [[ subsequence_list_lendu m 88 4 ;; indirectBlockPointer =>
                                                    [[ subsequence_list_lendu m 92 4 ;; doubleIndirectBlockPointer =>
                                                      [[ subsequence_list_lendu m 96 4 ;; tripleIndirectBlockPointer =>
                                                        [[ subsequence_list_lendu m 100 4 ;; generationNumber =>
                                                          [[ subsequence_list_lendu m 104 4 ;; extendedAttributeBlock =>
                                                            [[ subsequence_list_lendu m 108 4 ;; upperBitsFileSize =>
                                                              [[ subsequence_list_lendu m 112 4 ;; fragmentBlockAddress =>
                                                                [[ m 116 ;; fragmentIndexInBlock =>
                                                                  [[ m 117 ;; fragmentSize =>
                                                                    (* 118-119 Unused *)
                                                                    [[ subsequence_list_lendu m 120 2 ;; upperBitsUserId =>
                                                                      [[ subsequence_list_lendu m 122 2 ;; upperBitsGroupId |>
                                                                        (* 124-127 Unused *)
                                                                        (mk_ext2_inode_t 
                                                                          fileMode 
                                                                          lowerBitsUserId 
                                                                          lowerBitsFileSize 
                                                                          accessTime 
                                                                          changeTime
                                                                          modificationTime
                                                                          deletionTime
                                                                          lowerBitsGroupId
                                                                          linkCount
                                                                          sectorCount
                                                                          flags

                                                                          (directBlockPointer1 :: directBlockPointer2 :: directBlockPointer3 :: directBlockPointer4 ::
                                                                          directBlockPointer5 :: directBlockPointer6 :: directBlockPointer7 :: directBlockPointer8 ::
                                                                          directBlockPointer9 :: directBlockPointer10:: directBlockPointer11:: directBlockPointer12::
                                                                          nil)
                                                                         
                                                                          indirectBlockPointer
                                                                          doubleIndirectBlockPointer
                                                                          tripleIndirectBlockPointer
                                                                          generationNumber
                                                                          extendedAttributeBlock
                                                                          upperBitsFileSize
                                                                          fragmentBlockAddress
                                                                          fragmentIndexInBlock
                                                                          fragmentSize
                                                                          upperBitsUserId
                                                                          upperBitsGroupId
                                                                        )
                                                                      ]]
                                                                    ]]
                                                                  ]]
                                                                ]]
                                                              ]]
                                                            ]]
                                                          ]]
                                                        ]]
                                                      ]]
                                                    ]]
                                                  ]]
                                                ]]
                                              ]]
                                            ]]
                                          ]]
                                        ]]
                                      ]]
                                    ]]
                                  ]]
                                ]]
                              ]]
                            ]]
                          ]]
                        ]]
                      ]]
                    ]]
                  ]]
                ]]
              ]]
            ]]
          ]]
        ]]
      ]]
    ]]
  ]].

Function ext2_inode_from (disk : Disk) (inodeId : Z) : Exc ext2_inode_t :=
  [[ ext2_superblock_from disk ;; superblock =>
    let groupId := (inodeId / (numInodesInBlockGroup superblock)) in
    (* GroupId is 1 indexed *)
    [[ ext2_group_descriptor_at disk (1 + groupId) ;; groupDescriptor =>
      (* Inode Table is 1-indexed *)
      let inodeIndexInTable := ((inodeId - 1) mod (numInodesInBlockGroup superblock)) in
      (* Inode Table + offset for this particular inode, i.e. inode index * size-of-inode *)
      let inodePos := (blockAddr2Offset superblock (startBlockAddrOfInodeTable groupDescriptor)) + (inodeIndexInTable*128) in
      ext2_inode_parse (shift disk inodePos)
    ]]
  ]].

(* Recursive function for dealing with levels of indirection *)
Fixpoint walkIndirection (disk: Disk) (superblock: ext2_superblock_t) (blockNumber startIndirectionBlock: Z) 
                         (indirectionLevel: nat): Exc Z := match indirectionLevel with
| O => let bytePosition := (startIndirectionBlock + 4*blockNumber) in
  (subsequence_list_lendu disk bytePosition (bytePosition+4))
| S nextIndirectionLevel => 
  let exponent := Z.of_nat indirectionLevel in
  let unitSizeInBlocks := ((blockSize superblock) ^ (exponent)) / (4 ^ exponent) in
  let nextBlockIndex := blockNumber / unitSizeInBlocks in
  let nextBytePosition := (startIndirectionBlock + 4*nextBlockIndex) in
  [[ subsequence_list_lendu disk nextBytePosition (nextBytePosition+4) ;; nextBlockBA =>
    (walkIndirection disk 
                     superblock 
                     (blockNumber mod unitSizeInBlocks) 
                     (blockAddr2Offset superblock nextBlockBA) 
                     nextIndirectionLevel)
  ]]
end.

(* Fetch an arbitrary offset within a file as signified by the inode *)
Function ext2_inode_offset (disk: Disk) (inode: ext2_inode_t) (bytePos: Z): Exc Z :=
  [[ ext2_superblock_from disk ;; superblock =>
    let blockSize := (blockSize superblock) in
    let blockNumInFile := bytePos / blockSize in
    if bytePos <? (lowerBitsFileSize inode) then (* @TODO: Use full file size *)
      [[ (
        if blockNumInFile <=? 12 then
          nth_error (directBlockPointers inode) (Z.to_nat blockNumInFile)
        else if blockNumInFile <=? (12 + blockSize/4) then
          let offsetBlockNum := (blockNumInFile - 12) in
          walkIndirection disk superblock offsetBlockNum (blockAddr2Offset superblock (indirectBlockPointer inode)) O
        else if blockNumInFile <=? (12 + blockSize/4 + (blockSize/4)*(blockSize/4)) then
          let offsetBlockNum := (blockNumInFile - 12 - (blockSize/4)) in
          walkIndirection disk superblock offsetBlockNum (blockAddr2Offset superblock (doubleIndirectBlockPointer inode)) (S O)
        else
          let offsetBlockNum := (blockNumInFile - 12 - (blockSize/4) - (blockSize*blockSize/16)) in
          walkIndirection disk superblock offsetBlockNum (blockAddr2Offset superblock (tripleIndirectBlockPointer inode)) (S (S O))
      ) ;; blockAddress =>
        disk (blockSize * blockAddress + (bytePos mod blockSize))
      ]]
    else error
  ]]
.

Fixpoint build_file_data (disk: Disk) (inode: ext2_inode_t) 
  (relevantBytes: list Z): map_Z_Z :=
  match relevantBytes with
  | nil => MZ.empty Z
  | headByte :: tail => match (ext2_inode_offset disk inode headByte) with
    | value byte => (update (pair headByte byte) (build_file_data disk inode tail))
    | error => (build_file_data disk inode tail)
  end
end.

Function parseFileFromInodeIndex (disk: Disk) (inodeIndex: Z) 
  (relevantBytes: list Z): Exc File :=
  [[ Ext2IsDeletedInode inodeIndex disk ;; deleted =>
    [[ ext2_inode_from disk inodeIndex ;; inode |>
      mkFile
        (lowerBitsFileSize inode) (*  @TODO: Use the full file size *)
        deleted
        (mkDiskFrom (build_file_data disk inode relevantBytes))
    ]]
  ]]
.
