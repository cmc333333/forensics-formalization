Require Import Coq.PArith.BinPos.
Require Import Coq.ZArith.ZArith.
Require Import FSets.FMaps.
Require Import FSets.FMapAVL.
Require Export Coq.Structures.OrderedTypeEx.
Require Export List.

(* ======================================================================================= *)

(* notation for lists *)

Notation " [ ] " := nil : list_scope.
Notation " [ x ] " := (cons x nil) : list_scope.
Notation " [ x ; .. ; y ] " := (cons x .. (cons y nil) ..) : list_scope.

(* ======================================================================================= *)

(* instantiation of the AVL module that implements the finite map interface *)

Open Local Scope Z.

Module MZ := FSets.FMapAVL.Make Z_as_OT.

(* the type of finite maps from Z to Z *)
Definition map_Z_Z : Type := MZ.t Z.

(* lookup in a finite map *)
Definition find k (m : map_Z_Z) := MZ.find k m.

(* create a new finite map by adding/overwriting an element with an existing finite map *)
Definition update (p : Z * Z) (m : map_Z_Z) := MZ.add (fst p) (snd p) m.

(* notation for building finite maps *)
(* k |-> v   says that key k maps to value v *)
Notation "k |-> v" := (pair k v) (at level 60).
Notation "[ ]" := (MZ.empty Z).
Notation "[ p1 , .. , pn ]" := (update p1 .. (update pn []) .. ).

(* ======================================================================================= *)

(* combining Exc computations *)
Definition opt_flatmap {A B : Type} (opt : Exc A) (f : A -> Exc B) :=
  match opt with
    | Some v => f v
    | None => None
  end.

Definition opt_flatmap_value {A B : Type} (opt : Exc A) (f : A -> B) :=
  match opt with
    | Some v => value (f v)
    | None => None
  end.

(* notation to make it easier to read/write the combination of Exc computations, based on Chris' pattern (or Haskell do notation) *)
Notation "[[ e1 ;; x => e2 ]]" := (opt_flatmap e1 (fun x => e2)).
Notation "[[ e1 ;; x |> e2 ]]" := (opt_flatmap_value e1 (fun x => e2)).

(* example usage *)
Check [[ value 0 ;; x => value (x + 1) ]].
Compute [[ value 0 ;; x => value (x + 1) ]].

(* ======================================================================================= *)

(* now images will be represented by functions Z -> Exc Z, but those functions will be implemented using the AVL finite maps for
   efficiency *)

(* ======================================================================================= *)

(* from an image m given as a function Z -> Exc Z, extract the list of elements
  [ m(start); m(start+1); ...; m(start+length-1) ] *)
Definition subsequence_list (m : Z -> Exc Z) (start : Z) (length : Z) : Exc (list Z) :=
  match length with
    | Z0 => value nil
    | Zpos l => 
      (Pos.peano_rec
        (fun _ => Z -> Exc (list Z))
        (fun (start' : Z) => [[ m start' ;; x |> cons x nil ]])
        (fun _ (subsequence_list_aux_pred_l : Z -> Exc (list Z)) (start' : Z) => 
          [[ m start' ;; x => 
            [[ subsequence_list_aux_pred_l (Z.succ start') ;; xs |>
              cons x xs
            ]]
          ]]
        )
      )
      l
      start
    | Zneg _ => error
  end.

(* from an image m given as a function Z -> Exc Z, create a new image that is the same as 
   m for any offset in the list of intervals.  each interval is specified by a pair of
   start and length. *)
Fixpoint restrict (m : Z -> Exc Z) (intervals : list (Z * Z)) (n : Z) : Exc Z := 
  match intervals with
    | nil => error
    | (s, l) :: intervals' => if (s <=? n) && (n <? (s + l)) then m n else restrict m intervals' n
  end.

(* from an image m given as a function Z -> Exc Z, create a new image that is the same as 
   m, but the value at offset n in the original image is at (n + offset) in the new image. *)
Definition shift (m : Z -> Exc Z) (offset : Z) (n : Z) : Exc Z := m (n + offset).

(* helper function to restrict and shift an image given as a function Z -> Exc Z.  only one interval is used (specified by the
   starting offset and length) *)
Definition shift_restrict_one (m : Z -> Exc Z) (offset : Z) (length : Z) : Z -> Exc Z := restrict (shift m offset) (cons (0, length) nil).

(* ======================================================================================= *)

(* define an AVL finite map datastructure for the schardt image.  it only includes the bytes from offset 446 to offset 511
   inclusive from the schardt image.  these bytes represent the first partition table entry.  *)
Definition schardt_map : map_Z_Z := [ 446 |-> 128, 447 |-> 1, 448 |-> 1, 449 |-> 0, 450 |-> 7, 451 |-> 254, 452 |-> 191, 453 |-> 79, 454 |-> 63, 455 |-> 0, 456 |-> 0, 457 |-> 0, 458 |-> 17, 459 |-> 30, 460 |-> 145, 461 |-> 0, 510 |-> 85, 511 |-> 170 ]%Z.

(* now the finite map from Z to Z is turned into a function of type Z -> Exc Z *)
Definition schardt : Z -> Exc Z := fun (n : Z) => find n schardt_map.

(* we can compute the result of various operations on schardt.  note that when we ask for part of schardt that is not defined (is outside the domain / keys of the map), we get back none/error. *)
Compute (subsequence_list schardt 0 5).
Compute (subsequence_list schardt 446 0).
Compute (subsequence_list schardt 446 1).
Compute (subsequence_list schardt 446 2).
Compute (subsequence_list schardt 446 3).
Compute (subsequence_list schardt 446 4).
Compute (subsequence_list schardt 446 5).
Compute (subsequence_list schardt 446 16).
Compute (subsequence_list schardt 446 17).
Compute (subsequence_list schardt 446 18).

Compute (subsequence_list (restrict schardt (cons (446, 16) nil)) 446 16).
Compute (subsequence_list (restrict schardt (cons (446, 16) nil)) 0 16).

Compute (subsequence_list (restrict schardt (cons (446, 16) nil)) 0 16).

Compute (subsequence_list (shift_restrict_one schardt 446 16) 0 16).

(* ======================================================================================= *)

(* first attempt at specifying that the signature in a dos partition table is correct *)
Definition dos_partition_signature1 (m : Z -> Exc Z) : Exc bool := 
  [[ m 510 ;; n1 =>
    [[ m 511 ;; n2 |>
      (n1 =? 85) && (n2 =? 170)
    ]]
  ]].

(* second attempt at specifying that the signature in a dos partition table is correct *)
Definition dos_partition_signature2 (m : Z -> Exc Z) : Exc bool := 
  [[
    [[ m 510 ;; n1 =>
      [[ m 511 ;; n2 |>
        (n1, n2)
      ]]
    ]] ;; p |> 
    (fst p =? 85) && (snd p =? 170)
  ]].

(* third attempt at specifying that the signature in a dos partition table is correct *)
Definition dos_partition_signature3 (m : Z -> Exc Z) : Exc bool := 
  let sig : list Z := [ 85; 170 ]%Z in 
    [[ subsequence_list m 510 2 ;; sigCand |>
      if (list_eq_dec Z.eq_dec sigCand sig) then true else false
    ]].

(* the above definitions should perhaps be returning (Exc unit) instead of (Exc bool), since 
   error and (value false) are treated in the same way - both mean the signature is not correct *)

(* Prop-valued specification that a signature in a dos partition table is correct *)
Definition Dos_partition_signature (m : Z -> Exc Z) : Prop := 
  (* sig = 0xAA55 = [ 85; 170 ] in little endian *)
  (m 510 = value 85) /\ (m 511 = value 170).

(* prove that the (Exc bool) valued functions are sound.  that is, they imply the Prop-valued specification *)
Lemma dos_partition_signature_sound : 
  forall (m : Z -> Exc Z), dos_partition_signature1 m = value true -> Dos_partition_signature m.
Proof.
  intros m H.
  unfold dos_partition_signature1, Dos_partition_signature in *.
  destruct (m 510); simpl in *; try solve [ discriminate H ].
  destruct (m 511); simpl in *; try solve [ discriminate H ].
  injection H; clear H; intros H.
  rewrite andb_true_iff in H; destruct H as [ H1 H2 ].
  rewrite Z.eqb_eq in *; rewrite H1; rewrite H2; auto.
Qed.

(* now show that the schardt image has a correct signature in the partition table *)
Lemma dos_partition_signature_correct_schardt :   Dos_partition_signature schardt.
Proof.
  apply dos_partition_signature_sound.
  reflexivity.
Qed.

(* ======================================================================================= *)

(* a function to extract the nth entry from a dos partition table.  the resulting map has
   domain {0,1,...,15}.  *)
Definition dos_partition_table_primary_entry (m : Z -> Exc Z) (n : Z) : Exc (Z -> Exc Z) :=
  let primary_entry_length := 16%Z in
    let primary_entry_start := 446%Z in
      if andb (1 <=? n)%Z (n <=? 4)%Z 
        then 
          value (shift_restrict_one m (primary_entry_start + (primary_entry_length * (n - 1))) primary_entry_length)
        else error.

(* sample runs of the function above.  to make the result displayed we convert it into a list using subsequence_list. *)
Compute ([[ dos_partition_table_primary_entry schardt 0 ;; m |> subsequence_list m 0 16 ]]).
Compute ([[ dos_partition_table_primary_entry schardt 1 ;; m |> subsequence_list m 0 16 ]]).
Compute ([[ dos_partition_table_primary_entry schardt 2 ;; m |> subsequence_list m 0 16 ]]).
Compute ([[ dos_partition_table_primary_entry schardt 3 ;; m |> subsequence_list m 0 16 ]]).
Compute ([[ dos_partition_table_primary_entry schardt 4 ;; m |> subsequence_list m 0 16 ]]).
Compute ([[ dos_partition_table_primary_entry schardt 5 ;; m |> subsequence_list m 0 16 ]]).

(* ======================================================================================= *)

(* functions to convert lists of integers (representing octets in the range {0,1,...,255}) to integers *)

(* big endian unsigned *)
Fixpoint bendu_aux (acc : Z) (l : list Z) : Z := 
  match l with
    | nil => acc
    | a::l' => bendu_aux ((acc * 256) + a) l'
  end.

Definition bendu (l : list Z) : Z := 
  match l with
    | nil => 0%Z
    | a::l' => bendu_aux a l'
  end.

(* some simple tests of the big endian functions *)
Compute (bendu [1;0;0]%Z).
Compute (bendu [0;1;0]%Z).
Compute (bendu [0;0;1]%Z).
Compute (bendu (cons 0 nil)%Z).
Compute (bendu nil%Z).

(* little endian unsigned *)
Fixpoint lendu (l : list Z) : Z := 
match l with 
  | nil => 0%Z
  | a::l' => (a + (256 * lendu l'))%Z
end.   

(* some simple tests of the little endian functions *)
Compute (lendu [1;0;0]%Z).
Compute (lendu [0;1;0]%Z).
Compute (lendu [0;0;1]%Z).
Compute (lendu (cons 0 nil)%Z).
Compute (lendu nil%Z).

(* pull out an entry for primary partition 1 in a dos partition table, then calculate a number from the bytes at offsets 8,9,10,11
   (4 bytes).  this is the starting LBA and can be compared with the output of running mmls (from the sleuth kit) on schardt.img. *)
Compute (
  [[ dos_partition_table_primary_entry schardt 1 ;; m => 
    [[ subsequence_list m 8 4 ;; xs |>
      lendu xs
    ]]
  ]]
).

(* similarly, but extracts the length of partition 1 from schardt.img.  *)
Compute (
  [[ dos_partition_table_primary_entry schardt 1 ;; m => 
    [[ subsequence_list m 12 4 ;; xs |>
      lendu xs
    ]]
  ]]
).

(* ======================================================================================= *)

(* an auxiliary function to pull out a list of bytes from an image and perform a little endian conversion to an integer *)
Definition subsequence_list_lendu (m : Z -> Exc Z) (offset : Z) (length : Z) : Exc Z := [[ subsequence_list m offset length ;; xs |> lendu xs ]].

(* ======================================================================================= *)

(* a structure/record representing a primary partition table entry for dos partition tables.  corresponds to the structure on p89 of carrier's book.  *)
Structure dos_partition_table_primary_entry_t := mk_dos_partition_table_primary_entry_t {
  bootable : bool;
  startCHS : Z;
  partitionType : Z;
  endCHS : Z;
  startLBA : Z;
  sizeLBA : Z
}.

(* a function to parse a primary partition table entry from a dos partition table, and build a structure/record from it *)
Definition dos_partition_table_primary_entry_parse (m : Z -> Exc Z) : Exc dos_partition_table_primary_entry_t := 
  [[ m 0 ;; bootable_Z =>
    let bootable : bool := bootable_Z =? 128 in (* assumption: every value other than 128 is valid and means not bootable *)
      [[ subsequence_list_lendu m 1 3 ;; startCHS =>
        [[ m 4 ;; partitionType =>
          [[ subsequence_list_lendu m 5 3 ;; endCHS =>
            [[ subsequence_list_lendu m 8 4 ;; startLBA =>
              [[ subsequence_list_lendu m 12 4 ;; sizeLBA |>
                {|
                  bootable := bootable;
                  startCHS := startCHS;
                  partitionType := partitionType;
                  endCHS := endCHS;
                  startLBA := startLBA;
                  sizeLBA := sizeLBA 
                |}
            ]]
          ]]
        ]]
      ]]
    ]]
  ]].

(* try running the previously defined function *)
Compute (
  [[ dos_partition_table_primary_entry schardt 1 ;; m => 
    dos_partition_table_primary_entry_parse m
  ]]
).

(* define the semantics of the structure/record above, i.e., say when a structure/record matches a primary partition table entry.
   this is a Prop-valued definition, so it can use logical operators freely (although that doesn't make much difference in this simple case).  *)
Definition dos_partition_table_primary_entry_semantics (m : Z -> Exc Z) (model : dos_partition_table_primary_entry_t) : Prop :=
  (m 0 <> error) /\ (m 0 = value 128 <-> bootable model = true)
  /\ (subsequence_list_lendu m 1 3 = value (startCHS model))
  /\ (m 4 = value (partitionType model))
  /\ (subsequence_list_lendu m 5 3 = value (endCHS model))
  /\ (subsequence_list_lendu m 8 4 = value (startLBA model))
  /\ (subsequence_list_lendu m 12 4 = value (sizeLBA model))
.

(* prove that the result returned from calling dos_partition_table_primary_entry_parse yields a structure/record that
   has correct semantics.  this is proven for all images. *)
Lemma dos_partition_table_primary_entry_parse_sound : 
  forall (m : Z -> Exc Z) (model : dos_partition_table_primary_entry_t), 
    dos_partition_table_primary_entry_parse m = value model 
    -> 
    dos_partition_table_primary_entry_semantics m model.
Proof.
  unfold dos_partition_table_primary_entry_parse.
  unfold dos_partition_table_primary_entry_semantics.
  intros m model H.
  destruct (m 0); [ | discriminate H].
  destruct (subsequence_list_lendu m 1 3); [ | discriminate H].
  destruct (m 4); [ | discriminate H].
  destruct (subsequence_list_lendu m 5 3); [ | discriminate H].
  destruct (subsequence_list_lendu m 8 4); [ | discriminate H].
  destruct (subsequence_list_lendu m 12 4); [ | discriminate H].
  simpl in H.
  injection H; clear H; intros H; rewrite <- H; simpl; clear H; clear model.
  repeat (split); try solve [ discriminate ].
  intros H; injection H; clear H; intros H; rewrite H; clear H; reflexivity.
  rewrite Z.eqb_eq; intros H; rewrite H; reflexivity.
Qed.

(* prove that the schardt image in particular has a particular model.  note that the model appears in the statement of the lemma.
   the lemma is proved by using the dos_partition_table_primary_entry_parse_sound result from above, and then running/computing
   the dos_partition_table_primary_entry_parse function (which is done when call the reflexivity tactic.  this is a simple form of
   proof by reflection.  the idea here is that a forensics search tool can provide a very simple proof like the one below.  *)
Lemma schardt_model_correct : 
  exists m,
    (dos_partition_table_primary_entry schardt 1) = value m /\
    dos_partition_table_primary_entry_semantics 
    m
    {|
      bootable := true;
      startCHS := 257;
      partitionType := 7;
      endCHS := 5226494;
      startLBA := 63;
      sizeLBA := 9510417 
    |}.
Proof.
  eexists.
  split; [ reflexivity | apply dos_partition_table_primary_entry_parse_sound; reflexivity ].
Qed.

Print schardt_model_correct.

(* 

The proof term generated is shown below.  It is nice and short (a good thing)!

schardt_model_correct = 
ex_intro
  (fun m : Z -> Exc Z =>
   dos_partition_table_primary_entry schardt 1 = value m /\
   dos_partition_table_primary_entry_semantics m
     {|
     bootable := true;
     startCHS := 257;
     partitionType := 7;
     endCHS := 5226494;
     startLBA := 63;
     sizeLBA := 9510417 |})
  (shift_restrict_one schardt (446 + 16 * (1 - 1)) 16)
  (conj eq_refl
     (dos_partition_table_primary_entry_parse_sound
        (shift_restrict_one schardt (446 + 16 * (1 - 1)) 16)
        {|
        bootable := true;
        startCHS := 257;
        partitionType := 7;
        endCHS := 5226494;
        startLBA := 63;
        sizeLBA := 9510417 |} eq_refl))
     : exists m : Z -> Exc Z,
         dos_partition_table_primary_entry schardt 1 = value m /\
         dos_partition_table_primary_entry_semantics m
           {|
           bootable := true;
           startCHS := 257;
           partitionType := 7;
           endCHS := 5226494;
           startLBA := 63;
           sizeLBA := 9510417 |}

*)

(* ======================================================================================= *)

(*
java Ext2 /tmp/honeynet/honeypot.hda8.dd 
*)

(*
$ scala /r/forensics/code/logic/dump2.scala /tmp/honeynet/honeypot.hda8.dd honeypot_map 1024 1024 2048 $(( ( ( 265041 / 8192 ) + 1 ) * 32 )) > /tmp/coq_source.txt
*)

Definition honeypot_map : map_Z_Z := [ 1024 |-> 216, 1025 |-> 2, 1026 |-> 1, 1027 |-> 0, 1028 |-> 81, 1029 |-> 11, 1030 |-> 4, 1031 |-> 0, 1032 |-> 196, 1033 |-> 51, 1034 |-> 0, 1035 |-> 0, 1036 |-> 169, 1037 |-> 107, 1038 |-> 3, 1039 |-> 0, 1040 |-> 85, 1041 |-> 231, 1042 |-> 0, 1043 |-> 0, 1044 |-> 1, 1045 |-> 0, 1046 |-> 0, 1047 |-> 0, 1048 |-> 0, 1049 |-> 0, 1050 |-> 0, 1051 |-> 0, 1052 |-> 0, 1053 |-> 0, 1054 |-> 0, 1055 |-> 0, 1056 |-> 0, 1057 |-> 32, 1058 |-> 0, 1059 |-> 0, 1060 |-> 0, 1061 |-> 32, 1062 |-> 0, 1063 |-> 0, 1064 |-> 216, 1065 |-> 7, 1066 |-> 0, 1067 |-> 0, 1068 |-> 79, 1069 |-> 250, 1070 |-> 176, 1071 |-> 58, 1072 |-> 192, 1073 |-> 40, 1074 |-> 178, 1075 |-> 58, 1076 |-> 2, 1077 |-> 0, 1078 |-> 20, 1079 |-> 0, 1080 |-> 83, 1081 |-> 239, 1082 |-> 0, 1083 |-> 0, 1084 |-> 1, 1085 |-> 0, 1086 |-> 0, 1087 |-> 0, 1088 |-> 230, 1089 |-> 162, 1090 |-> 176, 1091 |-> 58, 1092 |-> 0, 1093 |-> 78, 1094 |-> 237, 1095 |-> 0, 1096 |-> 0, 1097 |-> 0, 1098 |-> 0, 1099 |-> 0, 1100 |-> 1, 1101 |-> 0, 1102 |-> 0, 1103 |-> 0, 1104 |-> 0, 1105 |-> 0, 1106 |-> 0, 1107 |-> 0, 1108 |-> 11, 1109 |-> 0, 1110 |-> 0, 1111 |-> 0, 1112 |-> 128, 1113 |-> 0, 1114 |-> 0, 1115 |-> 0, 1116 |-> 0, 1117 |-> 0, 1118 |-> 0, 1119 |-> 0, 1120 |-> 2, 1121 |-> 0, 1122 |-> 0, 1123 |-> 0, 1124 |-> 1, 1125 |-> 0, 1126 |-> 0, 1127 |-> 0, 1128 |-> 163, 1129 |-> 244, 1130 |-> 18, 1131 |-> 10, 1132 |-> 25, 1133 |-> 51, 1134 |-> 17, 1135 |-> 213, 1136 |-> 136, 1137 |-> 241, 1138 |-> 165, 1139 |-> 78, 1140 |-> 22, 1141 |-> 146, 1142 |-> 2, 1143 |-> 108, 1144 |-> 0, 1145 |-> 0, 1146 |-> 0, 1147 |-> 0, 1148 |-> 0, 1149 |-> 0, 1150 |-> 0, 1151 |-> 0, 1152 |-> 0, 1153 |-> 0, 1154 |-> 0, 1155 |-> 0, 1156 |-> 0, 1157 |-> 0, 1158 |-> 0, 1159 |-> 0, 1160 |-> 0, 1161 |-> 0, 1162 |-> 0, 1163 |-> 0, 1164 |-> 0, 1165 |-> 0, 1166 |-> 0, 1167 |-> 0, 1168 |-> 0, 1169 |-> 0, 1170 |-> 0, 1171 |-> 0, 1172 |-> 0, 1173 |-> 0, 1174 |-> 0, 1175 |-> 0, 1176 |-> 0, 1177 |-> 0, 1178 |-> 0, 1179 |-> 0, 1180 |-> 0, 1181 |-> 0, 1182 |-> 0, 1183 |-> 0, 1184 |-> 0, 1185 |-> 0, 1186 |-> 0, 1187 |-> 0, 1188 |-> 0, 1189 |-> 0, 1190 |-> 0, 1191 |-> 0, 1192 |-> 0, 1193 |-> 0, 1194 |-> 0, 1195 |-> 0, 1196 |-> 0, 1197 |-> 0, 1198 |-> 0, 1199 |-> 0, 1200 |-> 0, 1201 |-> 0, 1202 |-> 0, 1203 |-> 0, 1204 |-> 0, 1205 |-> 0, 1206 |-> 0, 1207 |-> 0, 1208 |-> 0, 1209 |-> 0, 1210 |-> 0, 1211 |-> 0, 1212 |-> 0, 1213 |-> 0, 1214 |-> 0, 1215 |-> 0, 1216 |-> 0, 1217 |-> 0, 1218 |-> 0, 1219 |-> 0, 1220 |-> 0, 1221 |-> 0, 1222 |-> 0, 1223 |-> 0, 1224 |-> 0, 1225 |-> 0, 1226 |-> 0, 1227 |-> 0, 1228 |-> 0, 1229 |-> 0, 1230 |-> 0, 1231 |-> 0, 1232 |-> 0, 1233 |-> 0, 1234 |-> 0, 1235 |-> 0, 1236 |-> 0, 1237 |-> 0, 1238 |-> 0, 1239 |-> 0, 1240 |-> 0, 1241 |-> 0, 1242 |-> 0, 1243 |-> 0, 1244 |-> 0, 1245 |-> 0, 1246 |-> 0, 1247 |-> 0, 1248 |-> 0, 1249 |-> 0, 1250 |-> 0, 1251 |-> 0, 1252 |-> 0, 1253 |-> 0, 1254 |-> 0, 1255 |-> 0, 1256 |-> 0, 1257 |-> 0, 1258 |-> 0, 1259 |-> 0, 1260 |-> 0, 1261 |-> 0, 1262 |-> 0, 1263 |-> 0, 1264 |-> 0, 1265 |-> 0, 1266 |-> 0, 1267 |-> 0, 1268 |-> 0, 1269 |-> 0, 1270 |-> 0, 1271 |-> 0, 1272 |-> 0, 1273 |-> 0, 1274 |-> 0, 1275 |-> 0, 1276 |-> 0, 1277 |-> 0, 1278 |-> 0, 1279 |-> 0, 1280 |-> 0, 1281 |-> 0, 1282 |-> 0, 1283 |-> 0, 1284 |-> 0, 1285 |-> 0, 1286 |-> 0, 1287 |-> 0, 1288 |-> 0, 1289 |-> 0, 1290 |-> 0, 1291 |-> 0, 1292 |-> 0, 1293 |-> 0, 1294 |-> 0, 1295 |-> 0, 1296 |-> 0, 1297 |-> 0, 1298 |-> 0, 1299 |-> 0, 1300 |-> 0, 1301 |-> 0, 1302 |-> 0, 1303 |-> 0, 1304 |-> 0, 1305 |-> 0, 1306 |-> 0, 1307 |-> 0, 1308 |-> 0, 1309 |-> 0, 1310 |-> 0, 1311 |-> 0, 1312 |-> 0, 1313 |-> 0, 1314 |-> 0, 1315 |-> 0, 1316 |-> 0, 1317 |-> 0, 1318 |-> 0, 1319 |-> 0, 1320 |-> 0, 1321 |-> 0, 1322 |-> 0, 1323 |-> 0, 1324 |-> 0, 1325 |-> 0, 1326 |-> 0, 1327 |-> 0, 1328 |-> 0, 1329 |-> 0, 1330 |-> 0, 1331 |-> 0, 1332 |-> 0, 1333 |-> 0, 1334 |-> 0, 1335 |-> 0, 1336 |-> 0, 1337 |-> 0, 1338 |-> 0, 1339 |-> 0, 1340 |-> 0, 1341 |-> 0, 1342 |-> 0, 1343 |-> 0, 1344 |-> 0, 1345 |-> 0, 1346 |-> 0, 1347 |-> 0, 1348 |-> 0, 1349 |-> 0, 1350 |-> 0, 1351 |-> 0, 1352 |-> 0, 1353 |-> 0, 1354 |-> 0, 1355 |-> 0, 1356 |-> 0, 1357 |-> 0, 1358 |-> 0, 1359 |-> 0, 1360 |-> 0, 1361 |-> 0, 1362 |-> 0, 1363 |-> 0, 1364 |-> 0, 1365 |-> 0, 1366 |-> 0, 1367 |-> 0, 1368 |-> 0, 1369 |-> 0, 1370 |-> 0, 1371 |-> 0, 1372 |-> 0, 1373 |-> 0, 1374 |-> 0, 1375 |-> 0, 1376 |-> 0, 1377 |-> 0, 1378 |-> 0, 1379 |-> 0, 1380 |-> 0, 1381 |-> 0, 1382 |-> 0, 1383 |-> 0, 1384 |-> 0, 1385 |-> 0, 1386 |-> 0, 1387 |-> 0, 1388 |-> 0, 1389 |-> 0, 1390 |-> 0, 1391 |-> 0, 1392 |-> 0, 1393 |-> 0, 1394 |-> 0, 1395 |-> 0, 1396 |-> 0, 1397 |-> 0, 1398 |-> 0, 1399 |-> 0, 1400 |-> 0, 1401 |-> 0, 1402 |-> 0, 1403 |-> 0, 1404 |-> 0, 1405 |-> 0, 1406 |-> 0, 1407 |-> 0, 1408 |-> 0, 1409 |-> 0, 1410 |-> 0, 1411 |-> 0, 1412 |-> 0, 1413 |-> 0, 1414 |-> 0, 1415 |-> 0, 1416 |-> 0, 1417 |-> 0, 1418 |-> 0, 1419 |-> 0, 1420 |-> 0, 1421 |-> 0, 1422 |-> 0, 1423 |-> 0, 1424 |-> 0, 1425 |-> 0, 1426 |-> 0, 1427 |-> 0, 1428 |-> 0, 1429 |-> 0, 1430 |-> 0, 1431 |-> 0, 1432 |-> 0, 1433 |-> 0, 1434 |-> 0, 1435 |-> 0, 1436 |-> 0, 1437 |-> 0, 1438 |-> 0, 1439 |-> 0, 1440 |-> 0, 1441 |-> 0, 1442 |-> 0, 1443 |-> 0, 1444 |-> 0, 1445 |-> 0, 1446 |-> 0, 1447 |-> 0, 1448 |-> 0, 1449 |-> 0, 1450 |-> 0, 1451 |-> 0, 1452 |-> 0, 1453 |-> 0, 1454 |-> 0, 1455 |-> 0, 1456 |-> 0, 1457 |-> 0, 1458 |-> 0, 1459 |-> 0, 1460 |-> 0, 1461 |-> 0, 1462 |-> 0, 1463 |-> 0, 1464 |-> 0, 1465 |-> 0, 1466 |-> 0, 1467 |-> 0, 1468 |-> 0, 1469 |-> 0, 1470 |-> 0, 1471 |-> 0, 1472 |-> 0, 1473 |-> 0, 1474 |-> 0, 1475 |-> 0, 1476 |-> 0, 1477 |-> 0, 1478 |-> 0, 1479 |-> 0, 1480 |-> 0, 1481 |-> 0, 1482 |-> 0, 1483 |-> 0, 1484 |-> 0, 1485 |-> 0, 1486 |-> 0, 1487 |-> 0, 1488 |-> 0, 1489 |-> 0, 1490 |-> 0, 1491 |-> 0, 1492 |-> 0, 1493 |-> 0, 1494 |-> 0, 1495 |-> 0, 1496 |-> 0, 1497 |-> 0, 1498 |-> 0, 1499 |-> 0, 1500 |-> 0, 1501 |-> 0, 1502 |-> 0, 1503 |-> 0, 1504 |-> 0, 1505 |-> 0, 1506 |-> 0, 1507 |-> 0, 1508 |-> 0, 1509 |-> 0, 1510 |-> 0, 1511 |-> 0, 1512 |-> 0, 1513 |-> 0, 1514 |-> 0, 1515 |-> 0, 1516 |-> 0, 1517 |-> 0, 1518 |-> 0, 1519 |-> 0, 1520 |-> 0, 1521 |-> 0, 1522 |-> 0, 1523 |-> 0, 1524 |-> 0, 1525 |-> 0, 1526 |-> 0, 1527 |-> 0, 1528 |-> 0, 1529 |-> 0, 1530 |-> 0, 1531 |-> 0, 1532 |-> 0, 1533 |-> 0, 1534 |-> 0, 1535 |-> 0, 1536 |-> 0, 1537 |-> 0, 1538 |-> 0, 1539 |-> 0, 1540 |-> 0, 1541 |-> 0, 1542 |-> 0, 1543 |-> 0, 1544 |-> 0, 1545 |-> 0, 1546 |-> 0, 1547 |-> 0, 1548 |-> 0, 1549 |-> 0, 1550 |-> 0, 1551 |-> 0, 1552 |-> 0, 1553 |-> 0, 1554 |-> 0, 1555 |-> 0, 1556 |-> 0, 1557 |-> 0, 1558 |-> 0, 1559 |-> 0, 1560 |-> 0, 1561 |-> 0, 1562 |-> 0, 1563 |-> 0, 1564 |-> 0, 1565 |-> 0, 1566 |-> 0, 1567 |-> 0, 1568 |-> 0, 1569 |-> 0, 1570 |-> 0, 1571 |-> 0, 1572 |-> 0, 1573 |-> 0, 1574 |-> 0, 1575 |-> 0, 1576 |-> 0, 1577 |-> 0, 1578 |-> 0, 1579 |-> 0, 1580 |-> 0, 1581 |-> 0, 1582 |-> 0, 1583 |-> 0, 1584 |-> 0, 1585 |-> 0, 1586 |-> 0, 1587 |-> 0, 1588 |-> 0, 1589 |-> 0, 1590 |-> 0, 1591 |-> 0, 1592 |-> 0, 1593 |-> 0, 1594 |-> 0, 1595 |-> 0, 1596 |-> 0, 1597 |-> 0, 1598 |-> 0, 1599 |-> 0, 1600 |-> 0, 1601 |-> 0, 1602 |-> 0, 1603 |-> 0, 1604 |-> 0, 1605 |-> 0, 1606 |-> 0, 1607 |-> 0, 1608 |-> 0, 1609 |-> 0, 1610 |-> 0, 1611 |-> 0, 1612 |-> 0, 1613 |-> 0, 1614 |-> 0, 1615 |-> 0, 1616 |-> 0, 1617 |-> 0, 1618 |-> 0, 1619 |-> 0, 1620 |-> 0, 1621 |-> 0, 1622 |-> 0, 1623 |-> 0, 1624 |-> 0, 1625 |-> 0, 1626 |-> 0, 1627 |-> 0, 1628 |-> 0, 1629 |-> 0, 1630 |-> 0, 1631 |-> 0, 1632 |-> 0, 1633 |-> 0, 1634 |-> 0, 1635 |-> 0, 1636 |-> 0, 1637 |-> 0, 1638 |-> 0, 1639 |-> 0, 1640 |-> 0, 1641 |-> 0, 1642 |-> 0, 1643 |-> 0, 1644 |-> 0, 1645 |-> 0, 1646 |-> 0, 1647 |-> 0, 1648 |-> 0, 1649 |-> 0, 1650 |-> 0, 1651 |-> 0, 1652 |-> 0, 1653 |-> 0, 1654 |-> 0, 1655 |-> 0, 1656 |-> 0, 1657 |-> 0, 1658 |-> 0, 1659 |-> 0, 1660 |-> 0, 1661 |-> 0, 1662 |-> 0, 1663 |-> 0, 1664 |-> 0, 1665 |-> 0, 1666 |-> 0, 1667 |-> 0, 1668 |-> 0, 1669 |-> 0, 1670 |-> 0, 1671 |-> 0, 1672 |-> 0, 1673 |-> 0, 1674 |-> 0, 1675 |-> 0, 1676 |-> 0, 1677 |-> 0, 1678 |-> 0, 1679 |-> 0, 1680 |-> 0, 1681 |-> 0, 1682 |-> 0, 1683 |-> 0, 1684 |-> 0, 1685 |-> 0, 1686 |-> 0, 1687 |-> 0, 1688 |-> 0, 1689 |-> 0, 1690 |-> 0, 1691 |-> 0, 1692 |-> 0, 1693 |-> 0, 1694 |-> 0, 1695 |-> 0, 1696 |-> 0, 1697 |-> 0, 1698 |-> 0, 1699 |-> 0, 1700 |-> 0, 1701 |-> 0, 1702 |-> 0, 1703 |-> 0, 1704 |-> 0, 1705 |-> 0, 1706 |-> 0, 1707 |-> 0, 1708 |-> 0, 1709 |-> 0, 1710 |-> 0, 1711 |-> 0, 1712 |-> 0, 1713 |-> 0, 1714 |-> 0, 1715 |-> 0, 1716 |-> 0, 1717 |-> 0, 1718 |-> 0, 1719 |-> 0, 1720 |-> 0, 1721 |-> 0, 1722 |-> 0, 1723 |-> 0, 1724 |-> 0, 1725 |-> 0, 1726 |-> 0, 1727 |-> 0, 1728 |-> 0, 1729 |-> 0, 1730 |-> 0, 1731 |-> 0, 1732 |-> 0, 1733 |-> 0, 1734 |-> 0, 1735 |-> 0, 1736 |-> 0, 1737 |-> 0, 1738 |-> 0, 1739 |-> 0, 1740 |-> 0, 1741 |-> 0, 1742 |-> 0, 1743 |-> 0, 1744 |-> 0, 1745 |-> 0, 1746 |-> 0, 1747 |-> 0, 1748 |-> 0, 1749 |-> 0, 1750 |-> 0, 1751 |-> 0, 1752 |-> 0, 1753 |-> 0, 1754 |-> 0, 1755 |-> 0, 1756 |-> 0, 1757 |-> 0, 1758 |-> 0, 1759 |-> 0, 1760 |-> 0, 1761 |-> 0, 1762 |-> 0, 1763 |-> 0, 1764 |-> 0, 1765 |-> 0, 1766 |-> 0, 1767 |-> 0, 1768 |-> 0, 1769 |-> 0, 1770 |-> 0, 1771 |-> 0, 1772 |-> 0, 1773 |-> 0, 1774 |-> 0, 1775 |-> 0, 1776 |-> 0, 1777 |-> 0, 1778 |-> 0, 1779 |-> 0, 1780 |-> 0, 1781 |-> 0, 1782 |-> 0, 1783 |-> 0, 1784 |-> 0, 1785 |-> 0, 1786 |-> 0, 1787 |-> 0, 1788 |-> 0, 1789 |-> 0, 1790 |-> 0, 1791 |-> 0, 1792 |-> 0, 1793 |-> 0, 1794 |-> 0, 1795 |-> 0, 1796 |-> 0, 1797 |-> 0, 1798 |-> 0, 1799 |-> 0, 1800 |-> 0, 1801 |-> 0, 1802 |-> 0, 1803 |-> 0, 1804 |-> 0, 1805 |-> 0, 1806 |-> 0, 1807 |-> 0, 1808 |-> 0, 1809 |-> 0, 1810 |-> 0, 1811 |-> 0, 1812 |-> 0, 1813 |-> 0, 1814 |-> 0, 1815 |-> 0, 1816 |-> 0, 1817 |-> 0, 1818 |-> 0, 1819 |-> 0, 1820 |-> 0, 1821 |-> 0, 1822 |-> 0, 1823 |-> 0, 1824 |-> 0, 1825 |-> 0, 1826 |-> 0, 1827 |-> 0, 1828 |-> 0, 1829 |-> 0, 1830 |-> 0, 1831 |-> 0, 1832 |-> 0, 1833 |-> 0, 1834 |-> 0, 1835 |-> 0, 1836 |-> 0, 1837 |-> 0, 1838 |-> 0, 1839 |-> 0, 1840 |-> 0, 1841 |-> 0, 1842 |-> 0, 1843 |-> 0, 1844 |-> 0, 1845 |-> 0, 1846 |-> 0, 1847 |-> 0, 1848 |-> 0, 1849 |-> 0, 1850 |-> 0, 1851 |-> 0, 1852 |-> 0, 1853 |-> 0, 1854 |-> 0, 1855 |-> 0, 1856 |-> 0, 1857 |-> 0, 1858 |-> 0, 1859 |-> 0, 1860 |-> 0, 1861 |-> 0, 1862 |-> 0, 1863 |-> 0, 1864 |-> 0, 1865 |-> 0, 1866 |-> 0, 1867 |-> 0, 1868 |-> 0, 1869 |-> 0, 1870 |-> 0, 1871 |-> 0, 1872 |-> 0, 1873 |-> 0, 1874 |-> 0, 1875 |-> 0, 1876 |-> 0, 1877 |-> 0, 1878 |-> 0, 1879 |-> 0, 1880 |-> 0, 1881 |-> 0, 1882 |-> 0, 1883 |-> 0, 1884 |-> 0, 1885 |-> 0, 1886 |-> 0, 1887 |-> 0, 1888 |-> 0, 1889 |-> 0, 1890 |-> 0, 1891 |-> 0, 1892 |-> 0, 1893 |-> 0, 1894 |-> 0, 1895 |-> 0, 1896 |-> 0, 1897 |-> 0, 1898 |-> 0, 1899 |-> 0, 1900 |-> 0, 1901 |-> 0, 1902 |-> 0, 1903 |-> 0, 1904 |-> 0, 1905 |-> 0, 1906 |-> 0, 1907 |-> 0, 1908 |-> 0, 1909 |-> 0, 1910 |-> 0, 1911 |-> 0, 1912 |-> 0, 1913 |-> 0, 1914 |-> 0, 1915 |-> 0, 1916 |-> 0, 1917 |-> 0, 1918 |-> 0, 1919 |-> 0, 1920 |-> 0, 1921 |-> 0, 1922 |-> 0, 1923 |-> 0, 1924 |-> 0, 1925 |-> 0, 1926 |-> 0, 1927 |-> 0, 1928 |-> 0, 1929 |-> 0, 1930 |-> 0, 1931 |-> 0, 1932 |-> 0, 1933 |-> 0, 1934 |-> 0, 1935 |-> 0, 1936 |-> 0, 1937 |-> 0, 1938 |-> 0, 1939 |-> 0, 1940 |-> 0, 1941 |-> 0, 1942 |-> 0, 1943 |-> 0, 1944 |-> 0, 1945 |-> 0, 1946 |-> 0, 1947 |-> 0, 1948 |-> 0, 1949 |-> 0, 1950 |-> 0, 1951 |-> 0, 1952 |-> 0, 1953 |-> 0, 1954 |-> 0, 1955 |-> 0, 1956 |-> 0, 1957 |-> 0, 1958 |-> 0, 1959 |-> 0, 1960 |-> 0, 1961 |-> 0, 1962 |-> 0, 1963 |-> 0, 1964 |-> 0, 1965 |-> 0, 1966 |-> 0, 1967 |-> 0, 1968 |-> 0, 1969 |-> 0, 1970 |-> 0, 1971 |-> 0, 1972 |-> 0, 1973 |-> 0, 1974 |-> 0, 1975 |-> 0, 1976 |-> 0, 1977 |-> 0, 1978 |-> 0, 1979 |-> 0, 1980 |-> 0, 1981 |-> 0, 1982 |-> 0, 1983 |-> 0, 1984 |-> 0, 1985 |-> 0, 1986 |-> 0, 1987 |-> 0, 1988 |-> 0, 1989 |-> 0, 1990 |-> 0, 1991 |-> 0, 1992 |-> 0, 1993 |-> 0, 1994 |-> 0, 1995 |-> 0, 1996 |-> 0, 1997 |-> 0, 1998 |-> 0, 1999 |-> 0, 2000 |-> 0, 2001 |-> 0, 2002 |-> 0, 2003 |-> 0, 2004 |-> 0, 2005 |-> 0, 2006 |-> 0, 2007 |-> 0, 2008 |-> 0, 2009 |-> 0, 2010 |-> 0, 2011 |-> 0, 2012 |-> 0, 2013 |-> 0, 2014 |-> 0, 2015 |-> 0, 2016 |-> 0, 2017 |-> 0, 2018 |-> 0, 2019 |-> 0, 2020 |-> 0, 2021 |-> 0, 2022 |-> 0, 2023 |-> 0, 2024 |-> 0, 2025 |-> 0, 2026 |-> 0, 2027 |-> 0, 2028 |-> 0, 2029 |-> 0, 2030 |-> 0, 2031 |-> 0, 2032 |-> 0, 2033 |-> 0, 2034 |-> 0, 2035 |-> 0, 2036 |-> 0, 2037 |-> 0, 2038 |-> 0, 2039 |-> 0, 2040 |-> 0, 2041 |-> 0, 2042 |-> 0, 2043 |-> 0, 2044 |-> 0, 2045 |-> 0, 2046 |-> 0, 2047 |-> 0, 2048 |-> 4, 2049 |-> 0, 2050 |-> 0, 2051 |-> 0, 2052 |-> 5, 2053 |-> 0, 2054 |-> 0, 2055 |-> 0, 2056 |-> 6, 2057 |-> 0, 2058 |-> 0, 2059 |-> 0, 2060 |-> 214, 2061 |-> 30, 2062 |-> 194, 2063 |-> 7, 2064 |-> 4, 2065 |-> 0, 2066 |-> 0, 2067 |-> 0, 2068 |-> 0, 2069 |-> 0, 2070 |-> 0, 2071 |-> 0, 2072 |-> 0, 2073 |-> 0, 2074 |-> 0, 2075 |-> 0, 2076 |-> 0, 2077 |-> 0, 2078 |-> 0, 2079 |-> 0, 2080 |-> 4, 2081 |-> 32, 2082 |-> 0, 2083 |-> 0, 2084 |-> 5, 2085 |-> 32, 2086 |-> 0, 2087 |-> 0, 2088 |-> 6, 2089 |-> 32, 2090 |-> 0, 2091 |-> 0, 2092 |-> 133, 2093 |-> 30, 2094 |-> 184, 2095 |-> 7, 2096 |-> 6, 2097 |-> 0, 2098 |-> 0, 2099 |-> 0, 2100 |-> 0, 2101 |-> 0, 2102 |-> 0, 2103 |-> 0, 2104 |-> 0, 2105 |-> 0, 2106 |-> 0, 2107 |-> 0, 2108 |-> 0, 2109 |-> 0, 2110 |-> 0, 2111 |-> 0, 2112 |-> 1, 2113 |-> 64, 2114 |-> 0, 2115 |-> 0, 2116 |-> 2, 2117 |-> 64, 2118 |-> 0, 2119 |-> 0, 2120 |-> 6, 2121 |-> 64, 2122 |-> 0, 2123 |-> 0, 2124 |-> 179, 2125 |-> 30, 2126 |-> 164, 2127 |-> 7, 2128 |-> 4, 2129 |-> 0, 2130 |-> 0, 2131 |-> 0, 2132 |-> 0, 2133 |-> 0, 2134 |-> 0, 2135 |-> 0, 2136 |-> 0, 2137 |-> 0, 2138 |-> 0, 2139 |-> 0, 2140 |-> 0, 2141 |-> 0, 2142 |-> 0, 2143 |-> 0, 2144 |-> 4, 2145 |-> 96, 2146 |-> 0, 2147 |-> 0, 2148 |-> 5, 2149 |-> 96, 2150 |-> 0, 2151 |-> 0, 2152 |-> 6, 2153 |-> 96, 2154 |-> 0, 2155 |-> 0, 2156 |-> 55, 2157 |-> 25, 2158 |-> 164, 2159 |-> 7, 2160 |-> 2, 2161 |-> 0, 2162 |-> 0, 2163 |-> 0, 2164 |-> 0, 2165 |-> 0, 2166 |-> 0, 2167 |-> 0, 2168 |-> 0, 2169 |-> 0, 2170 |-> 0, 2171 |-> 0, 2172 |-> 0, 2173 |-> 0, 2174 |-> 0, 2175 |-> 0, 2176 |-> 1, 2177 |-> 128, 2178 |-> 0, 2179 |-> 0, 2180 |-> 2, 2181 |-> 128, 2182 |-> 0, 2183 |-> 0, 2184 |-> 6, 2185 |-> 128, 2186 |-> 0, 2187 |-> 0, 2188 |-> 222, 2189 |-> 30, 2190 |-> 150, 2191 |-> 7, 2192 |-> 4, 2193 |-> 0, 2194 |-> 0, 2195 |-> 0, 2196 |-> 0, 2197 |-> 0, 2198 |-> 0, 2199 |-> 0, 2200 |-> 0, 2201 |-> 0, 2202 |-> 0, 2203 |-> 0, 2204 |-> 0, 2205 |-> 0, 2206 |-> 0, 2207 |-> 0, 2208 |-> 4, 2209 |-> 160, 2210 |-> 0, 2211 |-> 0, 2212 |-> 5, 2213 |-> 160, 2214 |-> 0, 2215 |-> 0, 2216 |-> 6, 2217 |-> 160, 2218 |-> 0, 2219 |-> 0, 2220 |-> 172, 2221 |-> 27, 2222 |-> 166, 2223 |-> 7, 2224 |-> 2, 2225 |-> 0, 2226 |-> 0, 2227 |-> 0, 2228 |-> 0, 2229 |-> 0, 2230 |-> 0, 2231 |-> 0, 2232 |-> 0, 2233 |-> 0, 2234 |-> 0, 2235 |-> 0, 2236 |-> 0, 2237 |-> 0, 2238 |-> 0, 2239 |-> 0, 2240 |-> 1, 2241 |-> 192, 2242 |-> 0, 2243 |-> 0, 2244 |-> 2, 2245 |-> 192, 2246 |-> 0, 2247 |-> 0, 2248 |-> 6, 2249 |-> 192, 2250 |-> 0, 2251 |-> 0, 2252 |-> 97, 2253 |-> 30, 2254 |-> 156, 2255 |-> 7, 2256 |-> 3, 2257 |-> 0, 2258 |-> 0, 2259 |-> 0, 2260 |-> 0, 2261 |-> 0, 2262 |-> 0, 2263 |-> 0, 2264 |-> 0, 2265 |-> 0, 2266 |-> 0, 2267 |-> 0, 2268 |-> 0, 2269 |-> 0, 2270 |-> 0, 2271 |-> 0, 2272 |-> 4, 2273 |-> 224, 2274 |-> 0, 2275 |-> 0, 2276 |-> 5, 2277 |-> 224, 2278 |-> 0, 2279 |-> 0, 2280 |-> 6, 2281 |-> 224, 2282 |-> 0, 2283 |-> 0, 2284 |-> 220, 2285 |-> 30, 2286 |-> 200, 2287 |-> 7, 2288 |-> 1, 2289 |-> 0, 2290 |-> 0, 2291 |-> 0, 2292 |-> 0, 2293 |-> 0, 2294 |-> 0, 2295 |-> 0, 2296 |-> 0, 2297 |-> 0, 2298 |-> 0, 2299 |-> 0, 2300 |-> 0, 2301 |-> 0, 2302 |-> 0, 2303 |-> 0, 2304 |-> 1, 2305 |-> 0, 2306 |-> 1, 2307 |-> 0, 2308 |-> 2, 2309 |-> 0, 2310 |-> 1, 2311 |-> 0, 2312 |-> 6, 2313 |-> 0, 2314 |-> 1, 2315 |-> 0, 2316 |-> 181, 2317 |-> 30, 2318 |-> 147, 2319 |-> 7, 2320 |-> 5, 2321 |-> 0, 2322 |-> 0, 2323 |-> 0, 2324 |-> 0, 2325 |-> 0, 2326 |-> 0, 2327 |-> 0, 2328 |-> 0, 2329 |-> 0, 2330 |-> 0, 2331 |-> 0, 2332 |-> 0, 2333 |-> 0, 2334 |-> 0, 2335 |-> 0, 2336 |-> 4, 2337 |-> 32, 2338 |-> 1, 2339 |-> 0, 2340 |-> 5, 2341 |-> 32, 2342 |-> 1, 2343 |-> 0, 2344 |-> 6, 2345 |-> 32, 2346 |-> 1, 2347 |-> 0, 2348 |-> 219, 2349 |-> 30, 2350 |-> 203, 2351 |-> 7, 2352 |-> 4, 2353 |-> 0, 2354 |-> 0, 2355 |-> 0, 2356 |-> 0, 2357 |-> 0, 2358 |-> 0, 2359 |-> 0, 2360 |-> 0, 2361 |-> 0, 2362 |-> 0, 2363 |-> 0, 2364 |-> 0, 2365 |-> 0, 2366 |-> 0, 2367 |-> 0, 2368 |-> 1, 2369 |-> 64, 2370 |-> 1, 2371 |-> 0, 2372 |-> 2, 2373 |-> 64, 2374 |-> 1, 2375 |-> 0, 2376 |-> 6, 2377 |-> 64, 2378 |-> 1, 2379 |-> 0, 2380 |-> 245, 2381 |-> 30, 2382 |-> 180, 2383 |-> 4, 2384 |-> 3, 2385 |-> 0, 2386 |-> 0, 2387 |-> 0, 2388 |-> 0, 2389 |-> 0, 2390 |-> 0, 2391 |-> 0, 2392 |-> 0, 2393 |-> 0, 2394 |-> 0, 2395 |-> 0, 2396 |-> 0, 2397 |-> 0, 2398 |-> 0, 2399 |-> 0, 2400 |-> 1, 2401 |-> 96, 2402 |-> 1, 2403 |-> 0, 2404 |-> 2, 2405 |-> 96, 2406 |-> 1, 2407 |-> 0, 2408 |-> 6, 2409 |-> 96, 2410 |-> 1, 2411 |-> 0, 2412 |-> 221, 2413 |-> 30, 2414 |-> 202, 2415 |-> 7, 2416 |-> 3, 2417 |-> 0, 2418 |-> 0, 2419 |-> 0, 2420 |-> 0, 2421 |-> 0, 2422 |-> 0, 2423 |-> 0, 2424 |-> 0, 2425 |-> 0, 2426 |-> 0, 2427 |-> 0, 2428 |-> 0, 2429 |-> 0, 2430 |-> 0, 2431 |-> 0, 2432 |-> 1, 2433 |-> 128, 2434 |-> 1, 2435 |-> 0, 2436 |-> 2, 2437 |-> 128, 2438 |-> 1, 2439 |-> 0, 2440 |-> 6, 2441 |-> 128, 2442 |-> 1, 2443 |-> 0, 2444 |-> 196, 2445 |-> 30, 2446 |-> 0, 2447 |-> 0, 2448 |-> 1, 2449 |-> 0, 2450 |-> 0, 2451 |-> 0, 2452 |-> 0, 2453 |-> 0, 2454 |-> 0, 2455 |-> 0, 2456 |-> 0, 2457 |-> 0, 2458 |-> 0, 2459 |-> 0, 2460 |-> 0, 2461 |-> 0, 2462 |-> 0, 2463 |-> 0, 2464 |-> 1, 2465 |-> 160, 2466 |-> 1, 2467 |-> 0, 2468 |-> 2, 2469 |-> 160, 2470 |-> 1, 2471 |-> 0, 2472 |-> 6, 2473 |-> 160, 2474 |-> 1, 2475 |-> 0, 2476 |-> 250, 2477 |-> 26, 2478 |-> 97, 2479 |-> 6, 2480 |-> 1, 2481 |-> 0, 2482 |-> 0, 2483 |-> 0, 2484 |-> 0, 2485 |-> 0, 2486 |-> 0, 2487 |-> 0, 2488 |-> 0, 2489 |-> 0, 2490 |-> 0, 2491 |-> 0, 2492 |-> 0, 2493 |-> 0, 2494 |-> 0, 2495 |-> 0, 2496 |-> 1, 2497 |-> 192, 2498 |-> 1, 2499 |-> 0, 2500 |-> 2, 2501 |-> 192, 2502 |-> 1, 2503 |-> 0, 2504 |-> 6, 2505 |-> 192, 2506 |-> 1, 2507 |-> 0, 2508 |-> 209, 2509 |-> 30, 2510 |-> 155, 2511 |-> 7, 2512 |-> 4, 2513 |-> 0, 2514 |-> 0, 2515 |-> 0, 2516 |-> 0, 2517 |-> 0, 2518 |-> 0, 2519 |-> 0, 2520 |-> 0, 2521 |-> 0, 2522 |-> 0, 2523 |-> 0, 2524 |-> 0, 2525 |-> 0, 2526 |-> 0, 2527 |-> 0, 2528 |-> 1, 2529 |-> 224, 2530 |-> 1, 2531 |-> 0, 2532 |-> 2, 2533 |-> 224, 2534 |-> 1, 2535 |-> 0, 2536 |-> 6, 2537 |-> 224, 2538 |-> 1, 2539 |-> 0, 2540 |-> 74, 2541 |-> 11, 2542 |-> 132, 2543 |-> 7, 2544 |-> 1, 2545 |-> 0, 2546 |-> 0, 2547 |-> 0, 2548 |-> 0, 2549 |-> 0, 2550 |-> 0, 2551 |-> 0, 2552 |-> 0, 2553 |-> 0, 2554 |-> 0, 2555 |-> 0, 2556 |-> 0, 2557 |-> 0, 2558 |-> 0, 2559 |-> 0, 2560 |-> 1, 2561 |-> 0, 2562 |-> 2, 2563 |-> 0, 2564 |-> 2, 2565 |-> 0, 2566 |-> 2, 2567 |-> 0, 2568 |-> 6, 2569 |-> 0, 2570 |-> 2, 2571 |-> 0, 2572 |-> 148, 2573 |-> 30, 2574 |-> 157, 2575 |-> 7, 2576 |-> 4, 2577 |-> 0, 2578 |-> 0, 2579 |-> 0, 2580 |-> 0, 2581 |-> 0, 2582 |-> 0, 2583 |-> 0, 2584 |-> 0, 2585 |-> 0, 2586 |-> 0, 2587 |-> 0, 2588 |-> 0, 2589 |-> 0, 2590 |-> 0, 2591 |-> 0, 2592 |-> 1, 2593 |-> 32, 2594 |-> 2, 2595 |-> 0, 2596 |-> 2, 2597 |-> 32, 2598 |-> 2, 2599 |-> 0, 2600 |-> 6, 2601 |-> 32, 2602 |-> 2, 2603 |-> 0, 2604 |-> 0, 2605 |-> 0, 2606 |-> 124, 2607 |-> 7, 2608 |-> 1, 2609 |-> 0, 2610 |-> 0, 2611 |-> 0, 2612 |-> 0, 2613 |-> 0, 2614 |-> 0, 2615 |-> 0, 2616 |-> 0, 2617 |-> 0, 2618 |-> 0, 2619 |-> 0, 2620 |-> 0, 2621 |-> 0, 2622 |-> 0, 2623 |-> 0, 2624 |-> 1, 2625 |-> 64, 2626 |-> 2, 2627 |-> 0, 2628 |-> 2, 2629 |-> 64, 2630 |-> 2, 2631 |-> 0, 2632 |-> 6, 2633 |-> 64, 2634 |-> 2, 2635 |-> 0, 2636 |-> 19, 2637 |-> 23, 2638 |-> 215, 2639 |-> 7, 2640 |-> 1, 2641 |-> 0, 2642 |-> 0, 2643 |-> 0, 2644 |-> 0, 2645 |-> 0, 2646 |-> 0, 2647 |-> 0, 2648 |-> 0, 2649 |-> 0, 2650 |-> 0, 2651 |-> 0, 2652 |-> 0, 2653 |-> 0, 2654 |-> 0, 2655 |-> 0, 2656 |-> 1, 2657 |-> 96, 2658 |-> 2, 2659 |-> 0, 2660 |-> 2, 2661 |-> 96, 2662 |-> 2, 2663 |-> 0, 2664 |-> 6, 2665 |-> 96, 2666 |-> 2, 2667 |-> 0, 2668 |-> 63, 2669 |-> 15, 2670 |-> 38, 2671 |-> 7, 2672 |-> 4, 2673 |-> 0, 2674 |-> 0, 2675 |-> 0, 2676 |-> 0, 2677 |-> 0, 2678 |-> 0, 2679 |-> 0, 2680 |-> 0, 2681 |-> 0, 2682 |-> 0, 2683 |-> 0, 2684 |-> 0, 2685 |-> 0, 2686 |-> 0, 2687 |-> 0, 2688 |-> 1, 2689 |-> 128, 2690 |-> 2, 2691 |-> 0, 2692 |-> 2, 2693 |-> 128, 2694 |-> 2, 2695 |-> 0, 2696 |-> 6, 2697 |-> 128, 2698 |-> 2, 2699 |-> 0, 2700 |-> 81, 2701 |-> 29, 2702 |-> 171, 2703 |-> 7, 2704 |-> 3, 2705 |-> 0, 2706 |-> 0, 2707 |-> 0, 2708 |-> 0, 2709 |-> 0, 2710 |-> 0, 2711 |-> 0, 2712 |-> 0, 2713 |-> 0, 2714 |-> 0, 2715 |-> 0, 2716 |-> 0, 2717 |-> 0, 2718 |-> 0, 2719 |-> 0, 2720 |-> 1, 2721 |-> 160, 2722 |-> 2, 2723 |-> 0, 2724 |-> 2, 2725 |-> 160, 2726 |-> 2, 2727 |-> 0, 2728 |-> 6, 2729 |-> 160, 2730 |-> 2, 2731 |-> 0, 2732 |-> 46, 2733 |-> 30, 2734 |-> 36, 2735 |-> 7, 2736 |-> 4, 2737 |-> 0, 2738 |-> 0, 2739 |-> 0, 2740 |-> 0, 2741 |-> 0, 2742 |-> 0, 2743 |-> 0, 2744 |-> 0, 2745 |-> 0, 2746 |-> 0, 2747 |-> 0, 2748 |-> 0, 2749 |-> 0, 2750 |-> 0, 2751 |-> 0, 2752 |-> 1, 2753 |-> 192, 2754 |-> 2, 2755 |-> 0, 2756 |-> 2, 2757 |-> 192, 2758 |-> 2, 2759 |-> 0, 2760 |-> 6, 2761 |-> 192, 2762 |-> 2, 2763 |-> 0, 2764 |-> 19, 2765 |-> 28, 2766 |-> 175, 2767 |-> 7, 2768 |-> 4, 2769 |-> 0, 2770 |-> 0, 2771 |-> 0, 2772 |-> 0, 2773 |-> 0, 2774 |-> 0, 2775 |-> 0, 2776 |-> 0, 2777 |-> 0, 2778 |-> 0, 2779 |-> 0, 2780 |-> 0, 2781 |-> 0, 2782 |-> 0, 2783 |-> 0, 2784 |-> 1, 2785 |-> 224, 2786 |-> 2, 2787 |-> 0, 2788 |-> 2, 2789 |-> 224, 2790 |-> 2, 2791 |-> 0, 2792 |-> 6, 2793 |-> 224, 2794 |-> 2, 2795 |-> 0, 2796 |-> 231, 2797 |-> 30, 2798 |-> 204, 2799 |-> 6, 2800 |-> 3, 2801 |-> 0, 2802 |-> 0, 2803 |-> 0, 2804 |-> 0, 2805 |-> 0, 2806 |-> 0, 2807 |-> 0, 2808 |-> 0, 2809 |-> 0, 2810 |-> 0, 2811 |-> 0, 2812 |-> 0, 2813 |-> 0, 2814 |-> 0, 2815 |-> 0, 2816 |-> 1, 2817 |-> 0, 2818 |-> 3, 2819 |-> 0, 2820 |-> 2, 2821 |-> 0, 2822 |-> 3, 2823 |-> 0, 2824 |-> 6, 2825 |-> 0, 2826 |-> 3, 2827 |-> 0, 2828 |-> 227, 2829 |-> 16, 2830 |-> 81, 2831 |-> 7, 2832 |-> 1, 2833 |-> 0, 2834 |-> 0, 2835 |-> 0, 2836 |-> 0, 2837 |-> 0, 2838 |-> 0, 2839 |-> 0, 2840 |-> 0, 2841 |-> 0, 2842 |-> 0, 2843 |-> 0, 2844 |-> 0, 2845 |-> 0, 2846 |-> 0, 2847 |-> 0, 2848 |-> 4, 2849 |-> 32, 2850 |-> 3, 2851 |-> 0, 2852 |-> 5, 2853 |-> 32, 2854 |-> 3, 2855 |-> 0, 2856 |-> 6, 2857 |-> 32, 2858 |-> 3, 2859 |-> 0, 2860 |-> 52, 2861 |-> 30, 2862 |-> 204, 2863 |-> 7, 2864 |-> 1, 2865 |-> 0, 2866 |-> 0, 2867 |-> 0, 2868 |-> 0, 2869 |-> 0, 2870 |-> 0, 2871 |-> 0, 2872 |-> 0, 2873 |-> 0, 2874 |-> 0, 2875 |-> 0, 2876 |-> 0, 2877 |-> 0, 2878 |-> 0, 2879 |-> 0, 2880 |-> 1, 2881 |-> 64, 2882 |-> 3, 2883 |-> 0, 2884 |-> 2, 2885 |-> 64, 2886 |-> 3, 2887 |-> 0, 2888 |-> 6, 2889 |-> 64, 2890 |-> 3, 2891 |-> 0, 2892 |-> 224, 2893 |-> 30, 2894 |-> 0, 2895 |-> 0, 2896 |-> 2, 2897 |-> 0, 2898 |-> 0, 2899 |-> 0, 2900 |-> 0, 2901 |-> 0, 2902 |-> 0, 2903 |-> 0, 2904 |-> 0, 2905 |-> 0, 2906 |-> 0, 2907 |-> 0, 2908 |-> 0, 2909 |-> 0, 2910 |-> 0, 2911 |-> 0, 2912 |-> 4, 2913 |-> 96, 2914 |-> 3, 2915 |-> 0, 2916 |-> 5, 2917 |-> 96, 2918 |-> 3, 2919 |-> 0, 2920 |-> 6, 2921 |-> 96, 2922 |-> 3, 2923 |-> 0, 2924 |-> 251, 2925 |-> 29, 2926 |-> 161, 2927 |-> 7, 2928 |-> 1, 2929 |-> 0, 2930 |-> 0, 2931 |-> 0, 2932 |-> 0, 2933 |-> 0, 2934 |-> 0, 2935 |-> 0, 2936 |-> 0, 2937 |-> 0, 2938 |-> 0, 2939 |-> 0, 2940 |-> 0, 2941 |-> 0, 2942 |-> 0, 2943 |-> 0, 2944 |-> 1, 2945 |-> 128, 2946 |-> 3, 2947 |-> 0, 2948 |-> 2, 2949 |-> 128, 2950 |-> 3, 2951 |-> 0, 2952 |-> 6, 2953 |-> 128, 2954 |-> 3, 2955 |-> 0, 2956 |-> 207, 2957 |-> 30, 2958 |-> 210, 2959 |-> 7, 2960 |-> 2, 2961 |-> 0, 2962 |-> 0, 2963 |-> 0, 2964 |-> 0, 2965 |-> 0, 2966 |-> 0, 2967 |-> 0, 2968 |-> 0, 2969 |-> 0, 2970 |-> 0, 2971 |-> 0, 2972 |-> 0, 2973 |-> 0, 2974 |-> 0, 2975 |-> 0, 2976 |-> 1, 2977 |-> 160, 2978 |-> 3, 2979 |-> 0, 2980 |-> 2, 2981 |-> 160, 2982 |-> 3, 2983 |-> 0, 2984 |-> 6, 2985 |-> 160, 2986 |-> 3, 2987 |-> 0, 2988 |-> 125, 2989 |-> 22, 2990 |-> 112, 2991 |-> 7, 2992 |-> 3, 2993 |-> 0, 2994 |-> 0, 2995 |-> 0, 2996 |-> 0, 2997 |-> 0, 2998 |-> 0, 2999 |-> 0, 3000 |-> 0, 3001 |-> 0, 3002 |-> 0, 3003 |-> 0, 3004 |-> 0, 3005 |-> 0, 3006 |-> 0, 3007 |-> 0, 3008 |-> 1, 3009 |-> 192, 3010 |-> 3, 3011 |-> 0, 3012 |-> 2, 3013 |-> 192, 3014 |-> 3, 3015 |-> 0, 3016 |-> 6, 3017 |-> 192, 3018 |-> 3, 3019 |-> 0, 3020 |-> 202, 3021 |-> 25, 3022 |-> 178, 3023 |-> 7, 3024 |-> 6, 3025 |-> 0, 3026 |-> 0, 3027 |-> 0, 3028 |-> 0, 3029 |-> 0, 3030 |-> 0, 3031 |-> 0, 3032 |-> 0, 3033 |-> 0, 3034 |-> 0, 3035 |-> 0, 3036 |-> 0, 3037 |-> 0, 3038 |-> 0, 3039 |-> 0, 3040 |-> 1, 3041 |-> 224, 3042 |-> 3, 3043 |-> 0, 3044 |-> 2, 3045 |-> 224, 3046 |-> 3, 3047 |-> 0, 3048 |-> 6, 3049 |-> 224, 3050 |-> 3, 3051 |-> 0, 3052 |-> 168, 3053 |-> 30, 3054 |-> 173, 3055 |-> 7, 3056 |-> 1, 3057 |-> 0, 3058 |-> 0, 3059 |-> 0, 3060 |-> 0, 3061 |-> 0, 3062 |-> 0, 3063 |-> 0, 3064 |-> 0, 3065 |-> 0, 3066 |-> 0, 3067 |-> 0, 3068 |-> 0, 3069 |-> 0, 3070 |-> 0, 3071 |-> 0, 3072 |-> 1, 3073 |-> 0, 3074 |-> 4, 3075 |-> 0, 3076 |-> 2, 3077 |-> 0, 3078 |-> 4, 3079 |-> 0, 3080 |-> 6, 3081 |-> 0, 3082 |-> 4, 3083 |-> 0, 3084 |-> 83, 3085 |-> 10, 3086 |-> 216, 3087 |-> 7, 3088 |-> 0, 3089 |-> 0, 3090 |-> 0, 3091 |-> 0, 3092 |-> 0, 3093 |-> 0, 3094 |-> 0, 3095 |-> 0, 3096 |-> 0, 3097 |-> 0, 3098 |-> 0, 3099 |-> 0, 3100 |-> 0, 3101 |-> 0, 3102 |-> 0, 3103 |-> 0, 263396 |-> 23, 263397 |-> 0, 263398 |-> 0, 263399 |-> 0, 263400 |-> 28, 263401 |-> 3, 263402 |-> 6, 263403 |-> 1, 263404 |-> 108, 263405 |-> 107, 263406 |-> 46, 263407 |-> 116, 263408 |-> 103, 263409 |-> 122 ]%Z.

(* now the finite map from Z to Z is turned into a function of type Z -> Exc Z *)
Definition honeypot : Z -> Exc Z := fun (n : Z) => find n honeypot_map.

(* ======================================================================================= *)

Structure ext2_superblock_t := mk_ext2_superblock_t {
  numInodes : Z;
  numBlocks : Z;
  numBlocksReserved : Z;
  numUnallocatedBlocks : Z;
  numUnallocatedInodes : Z;
  blockWhereGroupZeroStarts : Z;
  blockSize : Z;
  fragmentSize : Z;
  numBlocksInBlockGroup : Z;
  numFragmentsInBlockGroup : Z;
  numInodesInBlockGroup : Z;
  lastMountTime : Z;
  lastWrittenTime : Z;
  currentMountCount : Z;
  maxMountCount : Z;
  signature : Z;
  fileSystemState : Z;
  errorHandlingMethod : Z;
  minorVersion : Z;
  lastConsistencyCheckTime : Z;
  intervalBetweenForcedConsistencyChecks : Z;
  creatorOS : Z;
  majorVersion : Z;
  uidThatCanUseReservedBlocks : Z;
  gidThatCanUseReservedBlocks : Z;
  firstNonReservedInodeInFS : Z;
  sizeInodeStructure : Z;
  blockGroupThatThisSuperBlockIsPartOf : Z;
  compatibleFeatureFlags : Z;
  incompatibleFeatureFlags : Z;
  readOnlyFeatureFlags : Z;
  fileSystemID : list Z;
  volumeName : list Z;
  pathLastMountedOn : list Z;
  algorithmUsageBitmap : Z;
  numBlocksToPreallocateForFiles : Z;
  numBlocksToPreallocateForDirs : Z;
  journalID : Z;
  journalInode : Z;
  journalDevice : Z;
  headOfOrphanInodeList : Z
}.

(* a function to parse an Ext2 superblock, and build a structure/record from it *)
Definition ext2_superblock_parse (m : Z -> Exc Z) : Exc ext2_superblock_t := 
  [[ subsequence_list_lendu m 0 4 ;; numInodes =>
    [[ subsequence_list_lendu m 4 4 ;; numBlocks =>
      [[ subsequence_list_lendu m 8 4 ;; numBlocksReserved =>
        [[ subsequence_list_lendu m 12 4 ;; numUnallocatedBlocks =>
          [[ subsequence_list_lendu m 16 4 ;; numUnallocatedInodes =>
            [[ subsequence_list_lendu m 20 4 ;; blockWhereGroupZeroStarts =>
              [[ subsequence_list_lendu m 24 4 ;; blockSize =>
                [[ subsequence_list_lendu m 28 4 ;; fragmentSize =>
                  [[ subsequence_list_lendu m 32 4 ;; numBlocksInBlockGroup =>
                    [[ subsequence_list_lendu m 36 4 ;; numFragmentsInBlockGroup =>
                      [[ subsequence_list_lendu m 40 4 ;; numInodesInBlockGroup =>
                        [[ subsequence_list_lendu m 44 4 ;; lastMountTime =>
                          [[ subsequence_list_lendu m 48 4 ;; lastWrittenTime =>
                            [[ subsequence_list_lendu m 52 2 ;; currentMountCount =>
                              [[ subsequence_list_lendu m 54 2 ;; maxMountCount =>
                                [[ subsequence_list_lendu m 56 2 ;; signature =>
                                  [[ subsequence_list_lendu m 58 2 ;; fileSystemState =>
                                    [[ subsequence_list_lendu m 60 2 ;; errorHandlingMethod =>
                                      [[ subsequence_list_lendu m 62 2 ;; minorVersion =>
                                        [[ subsequence_list_lendu m 64 4 ;; lastConsistencyCheckTime =>
                                          [[ subsequence_list_lendu m 68 4 ;; intervalBetweenForcedConsistencyChecks =>
                                            [[ subsequence_list_lendu m 72 4 ;; creatorOS =>
                                              [[ subsequence_list_lendu m 76 4 ;; majorVersion =>
                                                [[ subsequence_list_lendu m 80 2 ;; uidThatCanUseReservedBlocks =>
                                                  [[ subsequence_list_lendu m 82 2 ;; gidThatCanUseReservedBlocks =>
                                                    [[ subsequence_list_lendu m 84 4 ;; firstNonReservedInodeInFS =>
                                                      [[ subsequence_list_lendu m 88 2 ;; sizeInodeStructure =>
                                                        [[ subsequence_list_lendu m 90 2 ;; blockGroupThatThisSuperBlockIsPartOf =>
                                                          [[ subsequence_list_lendu m 92 4 ;; compatibleFeatureFlags =>
                                                            [[ subsequence_list_lendu m 96 4 ;; incompatibleFeatureFlags =>
                                                              [[ subsequence_list_lendu m 100 4 ;; readOnlyFeatureFlags =>
                                                                [[ subsequence_list m 104 16 ;; fileSystemID =>
                                                                  [[ subsequence_list m 120 16 ;; volumeName =>
                                                                    [[ subsequence_list m 136 64 ;; pathLastMountedOn =>
                                                                      [[ subsequence_list_lendu m 200 4 ;; algorithmUsageBitmap =>
                                                                        [[ subsequence_list_lendu m 204 1 ;; numBlocksToPreallocateForFiles =>
                                                                          [[ subsequence_list_lendu m 205 1 ;; numBlocksToPreallocateForDirs =>
                                                                            [[ subsequence_list_lendu m 208 16 ;; journalID =>
                                                                              [[ subsequence_list_lendu m 224 4 ;; journalInode =>
                                                                                [[ subsequence_list_lendu m 228 4 ;; journalDevice =>
                                                                                  [[ subsequence_list_lendu m 232 4 ;; headOfOrphanInodeList |>
                                                                                    {|
                                                                                      numInodes := numInodes;
                                                                                      numBlocks := numBlocks;
                                                                                      numBlocksReserved := numBlocksReserved;
                                                                                      numUnallocatedBlocks := numUnallocatedBlocks;
                                                                                      numUnallocatedInodes := numUnallocatedInodes;
                                                                                      blockWhereGroupZeroStarts := blockWhereGroupZeroStarts;
                                                                                      blockSize := blockSize;
                                                                                      fragmentSize := fragmentSize;
                                                                                      numBlocksInBlockGroup := numBlocksInBlockGroup;
                                                                                      numFragmentsInBlockGroup := numFragmentsInBlockGroup;
                                                                                      numInodesInBlockGroup := numInodesInBlockGroup;
                                                                                      lastMountTime := lastMountTime;
                                                                                      lastWrittenTime := lastWrittenTime;
                                                                                      currentMountCount := currentMountCount;
                                                                                      maxMountCount := maxMountCount;
                                                                                      signature := signature;
                                                                                      fileSystemState := fileSystemState;
                                                                                      errorHandlingMethod := errorHandlingMethod;
                                                                                      minorVersion := minorVersion;
                                                                                      lastConsistencyCheckTime := lastConsistencyCheckTime;
                                                                                      intervalBetweenForcedConsistencyChecks := intervalBetweenForcedConsistencyChecks;
                                                                                      creatorOS := creatorOS;
                                                                                      majorVersion := majorVersion;
                                                                                      uidThatCanUseReservedBlocks := uidThatCanUseReservedBlocks;
                                                                                      gidThatCanUseReservedBlocks := gidThatCanUseReservedBlocks;
                                                                                      firstNonReservedInodeInFS := firstNonReservedInodeInFS;
                                                                                      sizeInodeStructure := sizeInodeStructure;
                                                                                      blockGroupThatThisSuperBlockIsPartOf := blockGroupThatThisSuperBlockIsPartOf;
                                                                                      compatibleFeatureFlags := compatibleFeatureFlags;
                                                                                      incompatibleFeatureFlags := incompatibleFeatureFlags;
                                                                                      readOnlyFeatureFlags := readOnlyFeatureFlags;
                                                                                      fileSystemID := fileSystemID;
                                                                                      volumeName := volumeName;
                                                                                      pathLastMountedOn := pathLastMountedOn;
                                                                                      algorithmUsageBitmap := algorithmUsageBitmap;
                                                                                      numBlocksToPreallocateForFiles := numBlocksToPreallocateForFiles;
                                                                                      numBlocksToPreallocateForDirs := numBlocksToPreallocateForDirs;
                                                                                      journalID := journalID;
                                                                                      journalDevice := journalDevice;
                                                                                      journalInode := journalInode;
                                                                                      headOfOrphanInodeList := headOfOrphanInodeList
                                                                                      |}
                                                                                  ]]
                                                                                ]]
                                                                              ]]
                                                                            ]]
                                                                          ]]
                                                                        ]]
                                                                      ]]
                                                                    ]]
                                                                  ]]
                                                                ]]
                                                              ]]
                                                            ]]
                                                          ]]
                                                        ]]
                                                      ]]
                                                    ]]
                                                  ]]
                                                ]]
                                              ]]
                                            ]]
                                          ]]
                                        ]]
                                      ]]
                                    ]]
                                  ]]
                                ]]
                              ]]
                            ]]
                          ]]
                        ]]
                      ]]
                    ]]
                  ]]
                ]]
              ]]
            ]]
          ]]
        ]]
      ]]
    ]]
  ]].

Compute (ext2_superblock_parse (shift honeypot 1024)).

(* ======================================================================================= *)

Definition ZtoN_exc (z : Z) : Exc N :=
  match Z.compare 0 z with
    | Eq => value (Z.to_N z)
    | Lt => value (Z.to_N z)
    | Gt => error
  end.

(* ======================================================================================= *)

Structure ext2_group_descriptor_t := mk_ext2_group_descriptor_t {
  startBlockAddrOfBlockBitmap : Z;
  startBlockAddrOfInodeBitmap : Z;
  startBlockAddrOfInodeTable : Z;
  numUnallocatedBlocksInGroup : Z;
  numUnallocatedInodesInGroup : Z;
  numDirsInGroup : Z;
  calcBlockGroupNumber : option Z;
  calcNumBlocksInGroup : option Z
}.

(* a function to parse a single Ext2 group descriptor, and build a structure/record from it *)
Definition ext2_group_descriptor_parse (m : Z -> Exc Z) : Exc ext2_group_descriptor_t := 
  [[ subsequence_list_lendu m 0 4 ;; startBlockAddrOfBlockBitmap =>
    [[ subsequence_list_lendu m 4 4 ;; startBlockAddrOfInodeBitmap =>
      [[ subsequence_list_lendu m 8 4 ;; startBlockAddrOfInodeTable =>
        [[ subsequence_list_lendu m 12 2 ;; numUnallocatedBlocksInGroup =>
          [[ subsequence_list_lendu m 14 2 ;; numUnallocatedInodesInGroup =>
            [[ subsequence_list_lendu m 16 2 ;; numDirsInGroup |>
              {|
                startBlockAddrOfBlockBitmap := startBlockAddrOfBlockBitmap;
                startBlockAddrOfInodeBitmap := startBlockAddrOfInodeBitmap;
                startBlockAddrOfInodeTable := startBlockAddrOfInodeTable;
                numUnallocatedBlocksInGroup := numUnallocatedBlocksInGroup;
                numUnallocatedInodesInGroup := numUnallocatedInodesInGroup;
                numDirsInGroup := numDirsInGroup;
                calcBlockGroupNumber := None;
                calcNumBlocksInGroup := None
                |}
            ]]
          ]]
        ]]
      ]]
    ]]
  ]].

Compute (ext2_group_descriptor_parse (shift honeypot (1024 + 1024))).

(* ======================================================================================= *)

Definition ext2_group_descriptor_entry_size : Z := 32.

Definition ext2_group_descriptor_update_block_group_number (g : ext2_group_descriptor_t) (calcBlockGroupNumber : option Z) : ext2_group_descriptor_t :=
  {|
    startBlockAddrOfBlockBitmap := g.(startBlockAddrOfBlockBitmap);
    startBlockAddrOfInodeBitmap := g.(startBlockAddrOfInodeBitmap);
    startBlockAddrOfInodeTable := g.(startBlockAddrOfInodeTable);
    numUnallocatedBlocksInGroup := g.(numUnallocatedBlocksInGroup);
    numUnallocatedInodesInGroup := g.(numUnallocatedInodesInGroup);
    numDirsInGroup := g.(numDirsInGroup);
    calcBlockGroupNumber := calcBlockGroupNumber;
    calcNumBlocksInGroup := g.(calcNumBlocksInGroup)
    |}.

Definition ext2_group_descriptor_update_num_blocks_in_group (g : ext2_group_descriptor_t) (calcNumBlocksInGroup : option Z) : ext2_group_descriptor_t :=
  {|
    startBlockAddrOfBlockBitmap := g.(startBlockAddrOfBlockBitmap);
    startBlockAddrOfInodeBitmap := g.(startBlockAddrOfInodeBitmap);
    startBlockAddrOfInodeTable := g.(startBlockAddrOfInodeTable);
    numUnallocatedBlocksInGroup := g.(numUnallocatedBlocksInGroup);
    numUnallocatedInodesInGroup := g.(numUnallocatedInodesInGroup);
    numDirsInGroup := g.(numDirsInGroup);
    calcBlockGroupNumber := g.(calcBlockGroupNumber);
    calcNumBlocksInGroup := calcNumBlocksInGroup
    |}.

(* TODO: is this definition sufficiently general?  Should we instead count up the number of blocks seen in each group, and then
compare the total so far with the number of blocks declared in the superblock? *)

Definition ext2_group_descriptor_table_parse (m : Z -> Exc Z) (numBlockGroups : Z) (numBlocks : Z) (numBlocksInBlockGroup : Z) : Exc (list ext2_group_descriptor_t) :=
  [[ ZtoN_exc numBlockGroups ;; numBlockGroupsN =>
    N.peano_rect
    (fun _ => Z -> Z -> (Z -> Exc Z) -> Exc (list ext2_group_descriptor_t))
    (fun (blockGroupNumber : Z) (blocksRemaining : Z) (m' : Z -> Exc Z) => value nil (* if (blocksRemaining =? 0) then value nil else error *) )
    (fun
      (numBlockGroupsp : N)
      (ext2_group_descriptor_table_parse_numBlockGroupsp : Z -> Z -> (Z -> Exc Z) -> Exc (list ext2_group_descriptor_t))
      (blockGroupNumber : Z) 
      (blocksRemaining : Z)
      (m' : Z -> Exc Z) => 
      let numBlocksInGroup : Z := Z.min blocksRemaining numBlocksInBlockGroup in
        [[ ext2_group_descriptor_table_parse_numBlockGroupsp (blockGroupNumber + 1) (blocksRemaining - numBlocksInBlockGroup) (shift m' ext2_group_descriptor_entry_size) ;; l =>
          [[ ext2_group_descriptor_parse m' ;; e |>
            let e' := ext2_group_descriptor_update_block_group_number e (Some blockGroupNumber) in
              let e'' := ext2_group_descriptor_update_num_blocks_in_group e' (Some numBlocksInGroup) in
                cons e'' l
          ]]
        ]]
    )
    (numBlockGroupsN)
    (0)
    (numBlocks)
    (m)
  ]].

(* note that the last block group may have fewer blocks than prior groups *)
Definition ext2_num_block_groups (numBlocks : Z) (numBlocksInBlockGroup : Z) : Exc Z := 
  if (numBlocks >=? 0) && (numBlocksInBlockGroup >? 0) 
    then 
      let d := numBlocks / numBlocksInBlockGroup in
        let r := numBlocks mod numBlocksInBlockGroup in
          if (r =? 0) 
            then
              value d
            else
              value (d + 1)
    else 
      error.

Definition ext2_superblock_and_group_descriptor_table_parse (m : Z -> Exc Z) : Exc (ext2_superblock_t * list ext2_group_descriptor_t) := 
  [[ ext2_superblock_parse (shift m 1024) ;; superblock =>
    [[ ext2_num_block_groups (superblock.(numBlocks)) (superblock.(numBlocksInBlockGroup)) ;; numBlockGroups =>
      [[ ext2_group_descriptor_table_parse (shift m (1024 + 1024)) numBlockGroups (superblock.(numBlocks)) (superblock.(numBlocksInBlockGroup)) ;; groupDescriptorTable |>
        (superblock, groupDescriptorTable)
      ]]
    ]]
  ]].

Compute (ext2_superblock_and_group_descriptor_table_parse honeypot).

Compute (
  [[ ext2_superblock_and_group_descriptor_table_parse honeypot ;; p |>
    snd p 
  ]]
).

Compute (
  [[ ext2_superblock_and_group_descriptor_table_parse honeypot ;; p |>
    length (snd p)
  ]]
).
  
(* ======================================================================================= *)

Structure ext2_directory_entry_t := mk_ext2_directory_entry_t
{
    inodeValue : Z ;
    lengthOfEntry : Z;
    nameLength : Z;
    fileType : Z;
    nameInAscii : list Z
}.

Definition ext2_directory_entry_parse (m : Z -> Exc Z) : Exc ext2_directory_entry_t := 
  [[ subsequence_list_lendu m 0 4 ;; inodeValue =>
    [[ subsequence_list_lendu m 4 2 ;; lengthOfEntry =>
      [[ subsequence_list_lendu m 6 1 ;; nameLength =>
        [[ subsequence_list_lendu m 7 1 ;; fileType =>
          [[ subsequence_list m 8 nameLength ;; nameInAscii |>
              {|
                  inodeValue := inodeValue;
                  lengthOfEntry := lengthOfEntry;
                  nameLength := nameLength;
                  fileType := fileType;
                  nameInAscii := nameInAscii
              |}
          ]]
        ]]
      ]]
    ]]
  ]].

Compute (ext2_directory_entry_parse (shift honeypot 263396)).

Definition ext2_get_inode_from_blockgroup (superblock : ext2_superblock_t) (gdt : list ext2_group_descriptor_t) (inode : Z) : Exc Z :=
match (gdt) with
    | nil => error
    | cons (gd) _ => value (gd.(startBlockAddrOfInodeTable) * (Z.shiftl 1024 superblock.(blockSize)) + superblock.(sizeInodeStructure) * inode)
end.

Compute (
  [[ ext2_superblock_and_group_descriptor_table_parse honeypot ;; p =>
     [[ ext2_get_inode_from_blockgroup (fst p) (snd p) 1 ;; n |>
        n
     ]]
  ]]
).



(*

what next?

ext2 or ntfs or fat?

try ext2
- get the partition out of the main image?  hmm, the honeynet example doesn't need it
- do the superblocks 

*)

(* ======================================================================================= *)


