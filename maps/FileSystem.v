Require Import Coq.ZArith.ZArith.

Require Import Disk.
Require Import File.
Require Import Inode.

Definition isOnDisk (file: File) (disk: Disk) :=
  (* Ext2 *)
  (exists (inodeIndex: Z) (byteOffsets: list Z),
    (parseFileFromInodeIndex disk inodeIndex byteOffsets) = value file)
.
