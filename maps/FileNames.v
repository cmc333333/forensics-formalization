Require Import Coq.Lists.List.
Require Import Coq.omega.Omega.
Require Import Coq.Strings.Ascii.
Require Import Coq.Strings.String.

Require Import Util.

Definition ByteString := list Z.

Function trimFileNamePrefix (fileName: ByteString): ByteString :=
  let reversedName := rev fileName in
  rev (takeWhile (fun (char: Z) => (negb (orb (char =? 47)    (* '/' *)
                                              (char =? 92)))) (* '\' *)
                 reversedName).

Fixpoint ascii2Bytes (fileName: string): ByteString :=
  match fileName with
  | EmptyString => nil
  | String char tail => (Z.of_N (N_of_ascii char)) :: (ascii2Bytes tail)
  end.

Local Open Scope string_scope.

Definition looksLikeRootkit (fileName: ByteString) :=
  In (trimFileNamePrefix fileName)
     (map ascii2Bytes ("ps" :: "netstat" :: "top" :: "ifconfig" :: "ssh" 
                       :: "rsync" :: "ProcMon.exe" :: nil)).

Lemma ex1 : looksLikeRootkit (ascii2Bytes "last/top").
  Proof.
  compute. right. right. left. reflexivity.
Qed.

Lemma ex2 : not (looksLikeRootkit (ascii2Bytes "last/example.txt")).
  Proof.
  compute. intro.
  repeat (destruct H; [ inversion H | ]).
  apply H.
Qed.
