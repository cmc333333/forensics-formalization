Require Export Coq.NArith.BinNat.

Require Export utils.

Local Open Scope N_scope. (* 1024 is now a binary number, N, rather than a nat *)

Inductive assump : Type := 
| ANth : forall (n : N) (v : N), assump
(*| AFile: forall (disk : list N) (offset :N) (file: list N), assump. *)
| ANotAnyRange : forall (range:N*N) (phi: ((list N)*N)->bool), assump
(*| ANotAny : forall (A:Type) (listFn : list N -> list A) (phi: A -> bool), assump *)
| ANthInt32 : forall (offset : N) (v : N), assump.

Definition assump_meaning (a : assump) (diskImage : list N) : Prop := match a with
| ANth n v => Nth_error N diskImage n = Some v
(*| AFile disk offset file => (*forall offsets in file, assert match *) *)
(*| ANotAny A listFn phi => forall (a:A), (In a (listFn diskImage) -> (phi a) = false)*)
| ANotAnyRange range phi => 
  forall (n:N), (andb ((fst range) <=? n) (n <? (snd range))) = true
    -> (phi (diskImage,n)) = false
| ANthInt32 offset v => int32 diskImage offset = Some v 
end.

Fixpoint CONJ (hs : list assump) (diskImage: list N): Prop := 
  match hs with
    | nil => True
    | h::hs' => assump_meaning h diskImage /\ CONJ hs' diskImage
  end.
