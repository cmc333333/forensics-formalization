Require Export Coq.NArith.BinNat.
Require Export SuperBlock.
Require Export GroupBlock.
Require Export FS.

Notation " [ ] " := nil : list_scope.
Notation " [ x ] " := (cons x nil) : list_scope.
Notation " [ x ; .. ; y ] " := (cons x .. (cons y nil) ..) : list_scope.

Local Open Scope N_scope. (* 1024 is now a binary number, N, rather than a nat *)

Structure INode := mkINode {
  fileLengthLower:N; (* Offset 4 *)
  directDataBAs: list N; (* Offset 40 - 87 *)
  indirectDataBA:N; (* Offset 88 *)
  dblIndirectDataBA:N; (* Offsets 92 *)
  tplIndirectDataBA:N (* Offsets 96 *)
}.

Function inodeFromSuperAndGroup (diskImage: list N) (inodeId: N) (superBlock: SuperBlock) (groupBlock: GroupBlock) :=
  let inodeIndexInGroup := inodeId mod (inodesPerGroup superBlock) in
  let inodePos := (ba2Pos superBlock (inodeTableBA groupBlock)) + (inodeIndexInGroup/128) in
  opt_flatmap (int32 diskImage (inodePos+4)) (fun fileLengthLower =>
    let int32_opts := map (fun i => int32 diskImage (inodePos+40 + 4*i)) [0;1;2;3;4;5;6;7;8;9;10;11] in
    let rev_int32s := fold_left (fun (so_far: option (list N)) (next : option N) => match (so_far, next) with
      | (None, _) => None
      | (_, None) => None
      | (Some sf, Some n) => Some (n :: sf)  (* reverses order *)
    end) int32_opts (Some []) in
    let int32s := opt_map rev_int32s (fun lst => rev lst) in
    opt_flatmap int32s (fun directDataBA =>
      opt_flatmap (int32 diskImage (inodePos+88)) (fun indirectDataBA =>
        opt_flatmap (int32 diskImage (inodePos+92)) (fun dblIndirectDataBA =>
          opt_flatmap (int32 diskImage (inodePos+96)) (fun tplIndirectDataBA =>
            Some (mkINode fileLengthLower directDataBA indirectDataBA dblIndirectDataBA tplIndirectDataBA)
  )))))
.

Function fetchInode (diskImage: list N) (inodeId: N) : option INode :=
  opt_flatmap (superBlockFrom diskImage) (fun superBlock =>
    let groupId := inodeId  / (inodesPerGroup superBlock) in
    opt_flatmap (groupBlockFrom diskImage groupId) (fun groupblock =>
      inodeFromSuperAndGroup diskImage inodeId superBlock groupblock
  ))
.

Fixpoint walkIndirection (diskImage: list N) (superBlock:SuperBlock) (blockNumber indirectionBA: N) (indirectionLevel: nat)
  : option N := match indirectionLevel with
| O => (int32 diskImage ((ba2Pos superBlock indirectionBA) + 4*blockNumber))
| S nextIndirectionLevel => 
  let exponent := N.of_nat indirectionLevel in
  let unitSizeInBlocks := ((blockSize superBlock) ^ (exponent)) / (4 ^ exponent) in
  let nextBlockIndex := blockNumber / unitSizeInBlocks in
  opt_flatmap (int32 diskImage ((ba2Pos superBlock indirectionBA) + 4*nextBlockIndex)) (fun nextBlockBA =>
    walkIndirection diskImage superBlock (blockNumber mod unitSizeInBlocks) nextBlockBA nextIndirectionLevel
  )
end.

Function ext2ByteOffset (diskImage: list N) (inode: INode) (superBlock: SuperBlock) (bytePos : N) : option N :=
  let blockSize := (blockSize superBlock) in
  let blockNumInFile := bytePos / blockSize in
  if bytePos <? (fileLengthLower inode) then 
    let blockAddressOpt := (
      if blockNumInFile <=? 12 then
        Nth_error N (directDataBAs inode) blockNumInFile
      else if blockNumInFile <=? (12 + blockSize/4) then
        let offsetBlockNum := (blockNumInFile - 12) in
        walkIndirection diskImage superBlock offsetBlockNum (indirectDataBA inode) O
      else if blockNumInFile <=? (12 + blockSize/4 + (blockSize/4)*(blockSize/4)) then
        let offsetBlockNum := (blockNumInFile - 12 - (blockSize/4)) in
        walkIndirection diskImage superBlock offsetBlockNum (dblIndirectDataBA inode) (S O)
      else
        let offsetBlockNum := (blockNumInFile - 12 - (blockSize/4) - (blockSize*blockSize/16)) in
        walkIndirection diskImage superBlock offsetBlockNum (tplIndirectDataBA inode) (S (S O))
    ) in
    opt_flatmap blockAddressOpt (fun blockAddress => 
      Nth_error N diskImage (blockSize * blockAddress + (bytePos mod blockSize))
    )
  else None
.

Function inode2File (inode: N) (diskImage: list N) : option File :=
  opt_flatmap (superBlockFrom diskImage) (fun superblock =>
    opt_map (fetchInode diskImage inode) (fun inodeStruct =>
      (mkFile (fileLengthLower inodeStruct) (fun byteIdx => 
        ext2ByteOffset diskImage inodeStruct superblock byteIdx
      ))
    )).
