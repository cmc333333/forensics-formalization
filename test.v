Require Export Coq.NArith.BinNat.
Require Export List.

Require Export utils.
Require Export assump.
Require Export SuperBlock.
Require Export GroupBlock.
Require Export INode.

Notation " [ ] " := nil : list_scope.
Notation " [ x ] " := (cons x nil) : list_scope.
Notation " [ x ; .. ; y ] " := (cons x .. (cons y nil) ..) : list_scope.

Local Open Scope N_scope. (* 1024 is now a binary number, N, rather than a nat *)

(* =========================================================== *)


(* =========================================================== *)

(* =========================================================== *)


(* =========================================================== *)

Definition rootDirIsAt (diskImage: list N) (offset: N) (* or block address? : N *):=
 (*stuff *)
 true.


Definition Ext2IsDeletedInode (inode : N) (diskImage: list N): bool := 
  match opt_flatmap (superBlockFrom diskImage) (fun superblock =>
    let groupId := inode / (inodesPerGroup superblock) in
    opt_flatmap (groupBlockFrom diskImage groupId) (fun groupblock =>
      let inodeIndexInGroup := inode mod (inodesPerGroup superblock) in
      let bitmapPos := (ba2Pos superblock (inodeBitmapBA groupblock)) in
      (* Byte associated with the INode Bitmap *)
      opt_map (Nth_error N diskImage (bitmapPos + (inodeIndexInGroup/8))) (fun inodeBitmapByte =>
        (andb
          (* Valid INode - Must be less than the number of INodes *)
          (inode <=? (maxINode superblock))

          (* The bit associated with this inode is 0 *)
          (negb (N.testbit inodeBitmapByte (inodeIndexInGroup mod 8)))
        )
  ))) with
 | Some v => v
 | None => false
 end
.

(* xxd hexdump *)

(* Invocation *)
(*
Lemma proof (inode: N) :
  exists hs : list assump, forall (diskImage: list N),
  CONJ hs diskImage ->
  Ext2IsDeletedInode inode diskImage.
  Proof.
*)
 
Lemma f : forall P:Prop, P -> P.
  refine ( fun (P:Prop) (H:P) => H ).
Qed.

Print f.

Lemma proofInstance:
  exists hs : list assump, forall (diskImage: list N),
  CONJ hs diskImage ->
  Ext2IsDeletedInode 23 diskImage = true.
  Proof.
  exists [ (* Example from http://old.honeynet.org/scans/scan15/ *)
  (* # INodes = 66264 *)
  ANthInt32 1024 (asInt32 [216;2;1;0]);
  (* Block size (1024 shifted this amount ) = 0 *)
  ANthInt32 1048 (asInt32 [0;0;0;0]);
  (* Blocks per group = 8192 *)
  ANthInt32 1056 (asInt32 [0;32;0;0]);
  (* INodes per group = 2008 *)
  ANthInt32 1064 (asInt32 [216;7;0;0]);
  (* INode Bitmap Block = 5 *)
  ANthInt32 2052 (asInt32 [5;0;0;0]);
  (* INode Table Block = 6 *)
  ANthInt32 2056 (asInt32 [6;0;0;0]);
  (* Bitmap Byte *)
  ANth 5122 63
  ].

  intros diskImage Ha.
  unfold Ext2IsDeletedInode.
  unfold CONJ in Ha. 
  intuition. 
  clear H7.
  simpl in H, H0, H1, H2, H3, H4, H5.
  unfold superBlockFrom.
  rewrite H, H0, H1, H2.
  clear H H0 H1 H2.

  unfold asInt32. unfold bytes2N. simpl bytes2Naux.
  unfold opt_flatmap. 
  simpl inodesPerGroup. simpl blockSizeExponent. simpl maxINode.

  unfold asInt32. unfold bytes2N. simpl bytes2Naux. 

  unfold groupBlockFrom. 
  simpl N.add at 1. rewrite H3. clear H3. 
  simpl N.add at 1. rewrite H4. clear H4.

  unfold asInt32. unfold bytes2N. simpl bytes2Naux.
  unfold opt_flatmap.
  simpl inodeBitmapBA.
  
  simpl N.shiftl. simpl N.mul. simpl N.add.
  rewrite H5. clear H5. 

  simpl. (* Final compute, now that diskImage does not appear *)
  reflexivity.
Qed.

(* =========================================================== *)

Print proofInstance.

(* =========================================================== *)
(* =========================================================== *)
(* =========================================================== *)


(*
Definition InodeJpeg (diskImage: list nat) (inode: N) : Prop :=
  opt_flatmap (superBlockFrom diskImage) 


Definition Ext2IsInodeContents (diskImage: list nat) (inode: N) (file: list nat): Prop := 
  match opt_flatmap (superBlockFrom diskImage) (fun superblock =>
    let groupId := inode / (inodesPerGroup superblock) in
    opt_flatmap (groupBlockFrom diskImage groupId) (fun groupblock =>
      let inodeIndexInGroup := inode mod (inodesPerGroup superblock) in
      opt_flatmap (inodeFrom diskImage (inodeTableBA groupblock) inodeIndexInGroup) (fun inode =>

  ))) with
  | Some v => v
  | None => False
  end
.
  
  exists hs : list assump, CONJ hs -> True.
*)

(*  @TODO

Definition IsBadArchiveImage (archiveImage : list nat) : Prop := exists hs : list assump, CONJ hs -> True.

Function extract (a : list nat) (f : list nat -> nat -> nat) (length : nat) : list nat := [] .
(* dummy definition above; should be :   map (f a) (0..(length-1)). *)

Definition theForensicsClaim (diskImage : list nat) : Prop :=
  exists (inode : N),
    exists (archiveImageGen : list nat -> nat -> nat),
      exists (archiveLength : nat),
        CONJ [ ANth nat diskImage 5 (N.to_nat 99) ]
        ->  
        let archiveImage : list nat := extract diskImage archiveImageGen archiveLength in
          Ext2IsDeletedInode diskImage inode (* Inode -> produces content -> feed that into isbadarchive *)
          /\ Ext2IsInodeContents diskImage inode archiveImage (* assumption which gets you the file contents. Write spec *)
          /\ IsBadArchiveImage archiveImage.


Lemma myProof : theForensicsClaim [ (* your disk image goes here *) ].
*)
