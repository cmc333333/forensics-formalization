Require Export Coq.NArith.BinNat.

Require Export utils.
Require Export assump.
Require Export SuperBlock.
Require Export INode.

Notation " [ ] " := nil : list_scope.
Notation " [ x ] " := (cons x nil) : list_scope.
Notation " [ x ; .. ; y ] " := (cons x .. (cons y nil) ..) : list_scope.

Local Open Scope N_scope. (* 1024 is now a binary number, N, rather than a nat *)

Definition isJpeg (file: File): bool :=
  match opt_flatmap (byteOffset file 0) (fun byte0 =>
  opt_flatmap (byteOffset file 1) (fun byte1 =>
  opt_flatmap (byteOffset file ((fileSize file) - 2)) (fun byteN2 =>
  opt_map (byteOffset file ((fileSize file) - 1)) (fun byteN1 =>
    (andb 
      (andb (byte0 =? 255) (byte1 =? 216)) 
      (andb (byteN2 =? 255) (byteN1 =? 217))
    )
  )))) with
| Some v => v
| None => false
end.

Lemma existsProofInstance:
  exists hs : list assump, forall (diskImage: list N),
  CONJ hs diskImage ->
  (opt_map (inode2File 20 diskImage) isJpeg) = Some true.
  Proof.
  exists [ (* Contrived Example *)
  (* # INodes = 66264 *)
  ANthInt32 1024 (asInt32 [216;2;1;0]);
  (* Block size (1024 shifted this amount ) = 0 *)
  ANthInt32 1048 (asInt32 [0;0;0;0]);
  (* Blocks per group = 8192 *)
  ANthInt32 1056 (asInt32 [0;32;0;0]);
  (* INodes per group = 2008 *)
  ANthInt32 1064 (asInt32 [216;7;0;0]);
  (* INode Bitmap Block = 5 *)
  ANthInt32 2052 (asInt32 [5;0;0;0]);
  (* INode Table Block = 6 *)
  ANthInt32 2056 (asInt32 [6;0;0;0]);

  (* INode File Size = 523205 *)
  ANthInt32 6148 (asInt32 [197;251;7;0]);
  (* INode Direct Bytes = 7-19 *)
  ANthInt32 6184 (asInt32 [7;0;0;0]);
  ANthInt32 6188 (asInt32 [8;0;0;0]);
  ANthInt32 6192 (asInt32 [9;0;0;0]);
  ANthInt32 6196 (asInt32 [10;0;0;0]);
  ANthInt32 6200 (asInt32 [11;0;0;0]);
  ANthInt32 6204 (asInt32 [12;0;0;0]);
  ANthInt32 6208 (asInt32 [13;0;0;0]);
  ANthInt32 6212 (asInt32 [14;0;0;0]);
  ANthInt32 6216 (asInt32 [15;0;0;0]);
  ANthInt32 6220 (asInt32 [16;0;0;0]);
  ANthInt32 6224 (asInt32 [17;0;0;0]);
  ANthInt32 6228 (asInt32 [18;0;0;0]);
  (* Indirect Pointer *)
  ANthInt32 6232 (asInt32 [19;0;0;0]);
  (* Dbl Indirect Pointer *)
  ANthInt32 6236 (asInt32 [20;0;0;0]);
  (* Triple Indirect Pointer *)
  ANthInt32 6240 (asInt32 [0;0;0;0]);

  (* Data *)
  ANth 7168 255;
  ANth 7169 216;
  (* Dbl Indirect Data *)
  ANthInt32 20480 (asInt32 [21;0;0;0]);
  ANthInt32 22472 (asInt32 [22;0;0;0]); (* 242th address in 21st block *)

  ANth 23491 255;
  ANth 23492 217
  ].
  
  intros diskImage Ha.
  unfold CONJ in Ha. intuition.
  clear H28.
  simpl in H, H0, H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14.
  simpl in H15, H16, H17, H18, H19, H20, H21, H22, H23, H24, H25, H26.
  
  unfold inode2File. unfold fetchInode. unfold superBlockFrom.
  rewrite H, H0, H1, H2.
  clear H H0 H1 H2.

  unfold asInt32. unfold bytes2N. simpl bytes2Naux.

  unfold opt_flatmap at 2 3 4 5.
  unfold opt_flatmap at 1.

  unfold opt_flatmap at 3 4 5 6. unfold opt_flatmap at 2. unfold opt_flatmap at 1.

  unfold groupBlockFrom.
  simpl N.add at 1. rewrite H3. clear H3.
  simpl N.add at 1. rewrite H4. clear H4.

  unfold opt_flatmap at 1 2.

  unfold inodeFromSuperAndGroup.
  unfold ba2Pos. unfold blockSize. simpl N.shiftl. simpl N.add at 1.
  rewrite H5. clear H5. unfold opt_flatmap at 1.

  simpl fold_left.
  rewrite H6. rewrite H7. rewrite H8. rewrite H9. rewrite H10. rewrite H11.
  rewrite H12. rewrite H13. rewrite H14. rewrite H15. rewrite H16. rewrite H17.
  clear H6 H7 H8 H9 H10 H11 H12 H13 H14 H15 H16 H17.
  unfold asInt32. unfold bytes2N. simpl bytes2Naux.

  unfold opt_map at 3. simpl option_map. unfold opt_flatmap at 1.

  simpl N.add at 1. rewrite H18. clear H18. unfold opt_flatmap at 1.
  simpl N.add at 1. rewrite H19. clear H19. unfold opt_flatmap at 1.
  simpl N.add at 1. rewrite H20. clear H20. unfold opt_flatmap at 1.

  unfold opt_map. simpl option_map.

  unfold isJpeg. unfold byteOffset at 1.

  unfold ext2ByteOffset at 1. simpl fileLengthLower. unfold blockSize at 1.
  simpl N.shiftl. simpl.

  rewrite H21. clear H21. unfold opt_flatmap at 1.

  unfold ext2ByteOffset at 1. simpl fileLengthLower. unfold blockSize at 1.
  simpl N.shiftl. simpl.

  rewrite H22. clear H22. unfold opt_flatmap at 1.
  unfold ext2ByteOffset at 1. simpl fileLengthLower. unfold blockSize at 1.
  simpl N.shiftl. 
  
  simpl directDataBAs. simpl walkIndirection. unfold ba2Pos.
  simpl Nth_error at 1.

  simpl N.add at 1.
  assert (523203 <? 523205 = true). intuition. rewrite H. clear H.
  assert (523203 / 1024 <=? 12 = false). intuition. rewrite H. clear H.
  assert (523203 / 1024 <=? 268 = false). intuition. rewrite H. clear H.

  simpl N.add at 1.
  assert (523203 / 1024 <=? 65804 = true). intuition. rewrite H. clear H.
  
  unfold blockSize. simpl N.shiftl. simpl N.add at 1.
  rewrite H23. unfold opt_flatmap at 3.

  simpl N.add at 1. rewrite H24. 
  unfold asInt32. unfold bytes2N. simpl bytes2Naux.

  unfold opt_flatmap at 2.

  simpl N.add at 1. rewrite H25. clear H25.

  unfold opt_flatmap.

  unfold ext2ByteOffset at 1. simpl fileLengthLower. unfold blockSize at 1.
  simpl N.shiftl. simpl.

  unfold ba2Pos. unfold blockSize. simpl N.shiftl. simpl N.add at 1.
  rewrite H23. clear H23. unfold opt_flatmap at 2. simpl N.add at 1.
  rewrite H24. clear H24. unfold opt_flatmap at 1.

  unfold asInt32. unfold bytes2N. simpl bytes2Naux. simpl N.add at 1.

  rewrite H26. clear H26. unfold opt_map. unfold option_map.

  simpl. reflexivity.
  Qed.

Lemma onlyJpegInstance:
  exists hs : list assump, forall (diskImage: list N),
  CONJ hs diskImage -> match (superBlockFrom diskImage) with
  | None => False
  | Some superblock => forall (inodeIndex: N), 
    inodeIndex <? (maxINode superblock) + 1 = true ->
    ((inodeIndex = 14)
      \/ (boolify (opt_map (inode2File inodeIndex diskImage) isJpeg) = false))
  end.
  Proof.
  exists [ (* Another Contrived Example *)
  ANotAnyRange (0,14) (fun diskXinode => boolify 
    (opt_map (inode2File (snd diskXinode) (fst diskXinode)) isJpeg));
  ANotAnyRange (15, 66265) (fun diskXinode => boolify
    (opt_map (inode2File (snd diskXinode) (fst diskXinode)) isJpeg));
  (* # INodes = 66264 *)
  ANthInt32 1024 (asInt32 [216;2;1;0]);
  (* Block size (1024 shifted this amount ) = 0 *)
  ANthInt32 1048 (asInt32 [0;0;0;0]);
  (* Blocks per group = 8192 *)
  ANthInt32 1056 (asInt32 [0;32;0;0]);
  (* INodes per group = 2008 *)
  ANthInt32 1064 (asInt32 [216;7;0;0])
  ].

  intros diskImage Ha.
  unfold CONJ in Ha.
  intuition.
  clear H6.
  simpl in H, H0, H1, H2, H3, H4.

  unfold superBlockFrom. 
  rewrite H0. clear H0.
  rewrite H2. clear H2.
  rewrite H3. clear H3.
  rewrite H4. clear H4.
  unfold asInt32. unfold bytes2N. simpl bytes2Naux. 
  unfold opt_flatmap. unfold maxINode. simpl N.add.

  intros inodeIndex HBound.

  remember (inodeIndex <? 14) as lt.
  symmetry in Heqlt.  destruct lt.
  assert (andb (0 <=? inodeIndex) (inodeIndex <? 14) = true).
  assert (0 <=? inodeIndex = true). apply N.leb_le. apply N.le_0_l.
  rewrite H0. simpl. apply Heqlt.
  apply H in H0. rewrite H0. apply or_intror. reflexivity.

  remember (inodeIndex =? 14) as eq.
  destruct eq. symmetry in Heqeq. rewrite N.eqb_eq in Heqeq.
  rewrite Heqeq. apply or_introl. reflexivity.

  assert (andb (15 <=? inodeIndex) (inodeIndex <? 66265) = true).

  rewrite HBound. 
  symmetry in Heqeq. apply Bool.andb_true_iff. split.
  rewrite N.ltb_ge in Heqlt.
  rewrite N.leb_le.
  rewrite N.eqb_neq in Heqeq.
  rewrite N.lt_eq_cases in Heqlt.
  destruct Heqlt; [
    clear Heqeq
    | contradict Heqeq; auto
  ].
  rewrite <- N.lt_succ_r.
  replace 15 with (N.succ 14); [ | reflexivity].
  rewrite <- N.succ_lt_mono.
  exact H0. reflexivity.

  apply H1 in H0. rewrite H0. apply or_intror. reflexivity.
  Qed.
