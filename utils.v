Require Export List.
Require Export Coq.NArith.BinNat.

Notation " [ ] " := nil : list_scope.
Notation " [ x ] " := (cons x nil) : list_scope.
Notation " [ x ; .. ; y ] " := (cons x .. (cons y nil) ..) : list_scope.

Local Open Scope N_scope. (* 1024 is now a binary number, N, rather than a nat *)

Definition opt_flatmap {A B : Type} (opt : option A) (f : A -> option B) := 
  match opt with
    | Some v => f v
    | None => None
  end.

Definition opt_map {A B:Type} opt (f:A->B) := option_map f opt.

(* Should never be expanded. *)
Definition Nth_error (A : Set) (l : list A) (n : N) : Exc A := nth_error l (N.to_nat n).

Fixpoint bytes2Naux (bytes : list N) (result : N) : N :=
  match bytes with
    | nil => result
    | h :: t => bytes2Naux t ((N.shiftl result 8) + h)
  end.

Definition bytes2N (bytes : list N) : N := bytes2Naux bytes 0.

Definition asInt32 (bytes : list N) : N := bytes2N (rev bytes).

Definition int32 (disk : list N) (offset : N) : Exc N :=
  match [Nth_error N disk offset; Nth_error N disk (offset + 1); Nth_error N disk (offset + 2); Nth_error N disk (offset + 3)] with
    | [Some byte0; Some byte1; Some byte2; Some byte3] => Some (asInt32 [byte0;byte1;byte2;byte3])
    | _ => None
   end.

(* Treat Nones like falses *)
Function boolify (opt: option bool) : bool := match opt with
| Some v => v
| None => false
end.
