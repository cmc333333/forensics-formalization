default:
	coqc utils
	coqc assump
	coqc FS
	coqc SuperBlock
	coqc GroupBlock
	coqc INode

clean:
	rm -f *.vo *.glob
