Require Export Coq.NArith.BinNat.

Structure File := mkFile {
  fileSize: N;
  byteOffset: N->option N
}.
