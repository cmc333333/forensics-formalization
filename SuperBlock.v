Require Export Coq.NArith.BinNat.

Require Export utils.

Local Open Scope N_scope. (* 1024 is now a binary number, N, rather than a nat *)

Structure SuperBlock := mkSuperBlock {
  maxINode:N; (* Offset 0 *)
  blockSizeExponent:N; (* Offset 24 *)
  blocksPerGroup:N; (* Offset 32 *)
  inodesPerGroup:N (* Offset 40 *)
}.

Function superBlockFrom (diskImage: list N) := 
  (* Max INode is first in the superblock, which starts at position 1024 *)
  (* This assumes the diskImage is at the start of the file system *)
  opt_flatmap (int32 diskImage 1024) (fun maxINode =>
    opt_flatmap (int32 diskImage 1048 (*1024+24*)) (fun blockSizeExponent =>
      (* Blocks per Group is a 4-byte value found in the superblock *)
      opt_flatmap (int32 diskImage 1056 (*1024+32*)) (fun blocksPerGroup =>
        (* INodes per Group is a 4-byte value found in the superblock *)
        opt_flatmap (int32 diskImage 1064 (*1024+40*)) (fun inodesPerGroup =>
          Some (mkSuperBlock maxINode blockSizeExponent blocksPerGroup inodesPerGroup)
        )
      )
    )
  ).

Function blockSize (superBlock: SuperBlock) := N.shiftl 1024 (blockSizeExponent superBlock).

Function ba2Pos (superBlock: SuperBlock) (blockAddress: N) := 2048 + (blockSize superBlock) * (blockAddress - 2).
